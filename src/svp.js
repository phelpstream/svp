  // Common
  export { default as clone } from "./functions/clone"
  export { default as filter } from "./functions/filter"
  export { default as normalize } from "./functions/normalize"
  export { default as reduce } from "./functions/reduce"
  export { default as each } from "./functions/each"
  export { default as dig } from "./functions/dig"
  export { default as times } from "./functions/times"
  export { default as entries } from "./functions/toEntries" // alternative naming
  export { default as length } from "./functions/length"
  export { default as len } from "./functions/length" // alternative naming
  export { default as keys } from "./functions/toKeys" // alternative naming
  export { default as values } from "./functions/toValues" // alternative naming
  export { default as split } from "./functions/split"
  export { default as stackValue } from "./functions/stackValue"
  export { default as remove } from "./functions/remove"

  // Conditional Statements
  export { default as equal } from "./functions/equal"
  export { default as and } from "./functions/and"
  export { default as or } from "./functions/or"

  // Promises
  export { default as promisesPipe } from "./functions/promisesPipe"

  // Filters
  export { default as filterNulls } from "./functions/filterNulls"
  export { default as filterEmpties } from "./functions/filterEmpties"

  // Object
  export { default as merge } from "./functions/merge"
  export { default as morph } from "./functions/morph"
  export { default as assign } from "./functions/assign"
  export { default as defaults } from "./functions/defaults"
  export { default as create } from "./functions/create"
  export { default as extend } from "./functions/extend"
  export { default as freeze } from "./functions/freeze"
  export { default as get } from "./functions/get"
  export { default as has } from "./functions/has"
  export { default as hasnt } from "./functions/hasnt"
  // export { default as set } from "./functions/set"
  export { default as sort } from "./functions/sort"
  export { default as stringify } from "./functions/stringify"
  export { default as sfy } from "./functions/stringify" // alternative naming
  export { default as unique } from "./functions/unique"
  export { default as pick } from "./functions/pick"

  // Array
  export { default as map } from "./functions/map"
  export { default as mapFlip } from "./functions/mapFlip"
  export { default as reverse } from "./functions/reverse"
  export { default as dedupe } from "./functions/dedupe"
  export { default as dedupeLight } from "./functions/dedupeLight"
  export { default as join } from "./functions/join"
  export { default as slice } from "./functions/slice"
  export { default as shift } from "./functions/shift"
  export { default as unshift } from "./functions/unshift"
  export { default as pop } from "./functions/pop"
  export { default as push } from "./functions/push"
  export { default as splice } from "./functions/splice"
  export { default as insert } from "./functions/insert"
  export { default as every } from "./functions/every"
  export { default as some } from "./functions/some"
  export { default as includes } from "./functions/includes"

  // String
  export { default as trunc } from "./functions/slice" // alternative naming
  export { default as concat } from "./functions/concat"
  export { default as joinFlip } from "./functions/joinFlip"
  export { default as lcase } from "./functions/lcase"
  export { default as lowercase } from "./functions/lcase" // alternative naming
  export { default as ucase } from "./functions/ucase"
  export { default as uppercase } from "./functions/ucase" // alternative naming
  export { default as replaceAt } from "./functions/replaceAt"
  export { default as trim } from "./functions/trim"
  export { default as compact } from "./functions/compact"
  export { default as capture } from "./functions/capture"
  export { default as match } from "./functions/match"
  export { default as surround } from "./functions/surround"

  // Number
  export { default as parseFloat } from "./functions/parseFloat"
  export { default as parseInt } from "./functions/parseInt"
  export { default as operation } from "./functions/operation"
  export { default as sum } from "./functions/sum"

  // Function
  export { default as curry } from "./functions/curry"
  export { default as unary } from "./functions/unary"
  export { default as binary } from "./functions/binary"
  export { default as partial } from "./functions/partial"
  export { default as partialRight } from "./functions/partialRight"
  export { default as pipe } from "./functions/pipe"
  export { default as compose } from "./functions/compose"
  export { default as flip } from "./functions/flip"
  export { default as invoke } from "./functions/invoke"
  export { default as not } from "./functions/not"
  export { default as predicate } from "./functions/predicate"
  export { default as promisify } from "./functions/promisify"

  // To
  export { default as toPath } from "./functions/toPath"
  export { default as toArray } from "./functions/toArray"
  export { default as toString } from "./functions/toString"
  export { default as toEntries } from "./functions/toEntries"
  export { default as toKeys } from "./functions/toKeys"
  export { default as toValues } from "./functions/toValues"

  // Is
  export { default as typeOf } from "./functions/typeOf"
  export { default as isSameType } from "./functions/isSameType"
  export { default as isString } from "./functions/isString"
  export { default as isArray } from "./functions/isArray"
  export { default as isObject } from "./functions/isObject"
  export { default as isNumber } from "./functions/isNumber"
  export { default as isInteger } from "./functions/isInteger"
  export { default as isBoolean } from "./functions/isBoolean"
  export { default as isSymbol } from "./functions/isSymbol"
  export { default as isType } from "./functions/isType"
  export { default as isNaN } from "./functions/isNaN"
  export { default as isMap } from "./functions/isMap"
  export { default as isSet } from "./functions/isSet"
  export { default as isKey } from "./functions/isKey"
  export { default as isNull } from "./functions/isNull"
  export { default as isNotNull } from "./functions/isNotNull"
  export { default as isOdd } from "./functions/isOdd"
  export { default as isEven } from "./functions/isEven"
  export { default as isIterable } from "./functions/isIterable"
  export { default as isFinite } from "./functions/isFinite"
  export { default as isFunction } from "./functions/isFunction"
  export { default as isEmpty } from "./functions/isEmpty"
  export { default as isNotEmpty } from "./functions/isNotEmpty"
  export { default as isStrNumber } from "./functions/isStrNumber"
  export { default as isJsonString } from "./functions/isJsonString"
  export { default as isLength } from "./functions/isLength"
  export { default as isDefined } from "./functions/isDefined"
  export { default as isNotDefined } from "./functions/isNotDefined"
  export { default as isAny } from "./functions/isAny"
  export { default as hasAny } from "./functions/isAny" // alternative naming

  // Throw
  export { default as throwIt } from "./functions/throwIt"
  export { default as throwError } from "./functions/throwError"
