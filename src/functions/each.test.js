import { expect } from "chai"

import each from "./each"

describe("each.js", () => {
  // Observing
  it("should iterate over a string", () => {
    let value = "mot"
    let iteratee = (v, k) => (k % 2 === 0 ? v.toUpperCase() : v)
    let iterateedValue = each(value, iteratee)
    expect(iterateedValue).to.be.eql("MoT")
  })
  it("should iterate over an array", () => {
    let value = [1, 2, 3]
    let iteratee = v => v
    let iterateedValue = each(value, iteratee)
    expect(iterateedValue).to.be.eql(value)
  })
  it("should expect each value of the first level of the array", () => {
    let value = [1, 2, 3]
    each(value, function(val, index) {
      expect(val).to.be.equal(value[index])
    })
  })
  it("should iterate over the first level of an object", () => {
    let value = { one: 1, two: 2, three: 3 }
    let iteratee = v => v
    let iterateedValue = each(value, iteratee)
    expect(iterateedValue).to.be.eql(value)
  })
  it("should expect each value over the first level of an object", () => {
    let value = { one: 1, two: 2, three: 3 }
    each(value, function(val, key) {
      expect(val).to.be.equal(value[key])
    })
  })
  it("should expect a new object after looping over the first level of an object", () => {
    let value = { one: 1, two: 2, three: 3 }
    let iteratee = (v, k, o) => v + 1 // eslint-disable-line no-unused-vars
    let iterateedValue = each(value, iteratee)
    expect(iterateedValue).to.be.eql({ one: 2, two: 3, three: 4 })
  })

  it("should increment each number in the array by 1", () => {
    let value = [1, 2, 3]
    let inc = a => a + 1
    expect(each(value, inc)).to.be.eql([2, 3, 4])
  })

  it("should not modify the original array", () => {
    let value = [1, 2, 3]
    let inc = a => a + 1
    each(value, inc)
    expect(value).to.be.eql([1, 2, 3])
  })

  it("should not modify the original object", () => {
    let value = { a: 1, b: 2, c: 3 }
    let inc = a => a + 1
    each(value, inc)
    expect(value).to.be.eql({ a: 1, b: 2, c: 3 })
  })

})
