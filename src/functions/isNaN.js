/**
 * Check if a supposed number is actually really one.
 * @param {Number} supposedNb Result of a convertion (String>Number for ex).
 */
export default function isNaN(supposedNb) {
  return Number.isNaN(supposedNb) && Object.is(supposedNb, NaN)
}
