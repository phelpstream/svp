"use strict"
import { expect } from "chai"

import get from "./get"

describe("get.js", () => {
  it("should get a 1 level deep key from an object", () => {
    let obj = {
      first: true
    }
    expect(get(obj, "first")).to.be.equal(true)
  })

  it("should get a 2 level deep key from an object", () => {
    let obj = {
      first: true,
      second: {
        level: true
      }
    }
    expect(get(obj, "second.level")).to.be.equal(true)
  })

  it("should get a 3 level deep key from an object", () => {
    let obj = {
      first: true,
      second: {
        level: {
          arr: ["first", "second", "third"]
        }
      }
    }
    expect(get(obj, "second.level.arr[2]")).to.be.equal("third")
  })

  it("should get a 1 level deep key with dots in it from an object", () => {
    let obj = {
      "0.0.1": true
    }
    expect(get(obj, "0.0.1")).to.be.equal(true)
  })
})
