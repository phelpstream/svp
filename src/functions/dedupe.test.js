import { expect } from "chai"

import dedupe from "./dedupe"

describe("dedupe.js", () => {
  it("should dedupe an array of integers", () => {
    let a = [1, 2, 2, 3, 3, 4, 3, 4, 5, 5]
    expect(dedupe(a)).to.be.eql([1, 2, 3, 4, 5])
  })
  it("should dedupe an array of objects", () => {
    let a = [
      {
        one: 1,
        three: 3,
        two: 2
      },
      {
        two: 2,
        three: 3,
        one: 1
      },
      {
        three: 3,
        two: 2,
        one: 1
      }
    ]
    expect(dedupe(a)).to.be.eql([
      {
        one: 1,
        three: 3,
        two: 2
      }
    ])
  })
})
