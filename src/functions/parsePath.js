import { isArray, isKey } from "./is"
import { toPath, toString } from "./to"

export default function parsePath(value, object) {
  if (isArray(value)) return value
  return isKey(value, object) ? [value] : toPath(toString(value))
}

// let p = "to.a.key.0.in.array"
// console.log(parsePath(p))
