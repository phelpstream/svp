"use strict"
export default function invoke(method, ...args) {
  return function invokee(methodParent) {
    return methodParent[method].apply(methodParent, ...args)
  }
}
