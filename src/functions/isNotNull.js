import isNull from "./isNull"
import not from "./not"

export default not(isNull)
