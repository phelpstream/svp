import assign from "./assign"
import clone from "./clone"

export default function defaults(targetObj, ...sourceObjects) {
  let targetObjClone = clone(targetObj)
  return assign(targetObjClone, ...sourceObjects)
}

// let o = {
//   a: 1,
//   b: 2
// }

// let def = {
//   a: null,
//   b: null,
//   c: null
// }

// let r = defaults(def, o)
// console.log(r);
// console.log(def);
