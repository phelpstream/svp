import isDefined from "./isDefined"

export default function every(value, everyFn = (value, index, array) => isDefined(value)) { // eslint-disable-line no-unused-vars
  return value.every(everyFn)
}
