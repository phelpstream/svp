import { expect } from "chai"

import toString from "./toString"

describe("toString.js", () => {
  it("should convert to string the number", () => {
    expect(toString(2)).to.equal("2")
  })
})
