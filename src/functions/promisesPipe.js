//@ts-check
/* eslint-disable */
import get from "./get"
import stackValue from "./stackValue"
import isArray from "./isArray"

let defaultColor = "\x1b[0m"
let colors = {
  green: "\x1b[32m",
  red: "\x1b[31m",
  blue: "\x1b[34m"
}
let color = (color, text) => `${colors[color]}${text}${defaultColor}`

export default function promisesPipe(options) {
  if (!(this instanceof promisesPipe)) return new promisesPipe(options)
  let activeStack = get(options, "stack", false)
  let activeLogs = get(options, "logs", false)
  let stack = stackValue()
  let localChain = Promise.resolve()

  let stackArgs = args => {
    if (activeStack) stack.value(args)
    return args
  }
  let thenResponse = fnName => (...args) => {
    if (activeLogs) console.log(`\n${color("green", "[OK]")} Function: ${color("green", fnName)} :: Args: ${color("green", args)}`)
    return { ok: true, functionName: fnName, args }
  }
  let catchResponse = fnName => error => {
    if (activeLogs) console.log(`\n${color("red", "[ERROR]")} Function: ${color("red", fnName)} :: Error: ${color("red", error)}`)
    return { ok: false, functionName: fnName, error }
  }
  let promisify = fn => async (...args) => {
    try {
      return Promise.resolve(await fn(...args))
    } catch (error) {
      return Promise.reject(`${error}`)
    }
  }

  this.then = function(thenFn) {
    localChain = localChain.then(thenFn)
    return this
  }

  this.input = function(input, inputName = "input") {
    if (input) {
      if (isArray(input)) {
        localChain = Promise.resolve(...input)
      } else {
        localChain = Promise.resolve(input)
      }
      localChain = localChain.then(thenResponse(inputName))
    } else {
      localChain = Promise.reject("No input.").catch(catchResponse(inputName))
    }
    return this
  }

  this.logs = function(active = false) {
    activeLogs = active
    return this
  }

  this.log = function() {
    localChain.then(console.log)
    return this
  }

  this.sandbox = function(checkFn) {
    localChain.then(checkFn)
    return this
  }

  this.start = function(startPromisedFn, ...args) {
    let fnName = startPromisedFn.name
    localChain = startPromisedFn(...args).then(thenResponse(fnName && fnName.length > 0 ? fnName : "'startPromisedFn'"))
    return this
  }

  this.next = function(nextFn) {
    let fnName = nextFn.name
    this.then(async response => {
      let ok = get(response, "ok", false)
      let args = get(response, "args")
      if (ok) {
        return promisify(nextFn)(...args).then(thenResponse(fnName))
      } else {
        if (activeLogs)
          console.log(`\n${color("red", "[ERROR]")} Function: ${color("red", fnName)} :: Error: ${color("red", "See previous function.")}`)
        return Promise.resolve(response)
      }
    })
      .catch(catchResponse(fnName)) // if any error
      .then(stackArgs)
    return this
  }

  this.end = function() {
    return localChain
  }

  this.catch = function(catchFn) {
    localChain = localChain.catch(catchFn)
    return this
  }

  this.error = function(errorFn) {
    localChain.then(({ ok, error, functionName } = {}) => {
      if (ok === false) errorFn(error, functionName)
    })
    return this
  }

  this.value = function() {
    this.then(({ ok, args, error, functionName } = {}) => (ok ? { ok, data: get(args, 0) } : { ok, error, functionName }))
    return this
  }

  this.stack = function() {
    return stack.history()
  }

  return this
}

// test

// ;(async function() {
//   let pp = promisesPipe({ stack: true, logs: false })

//   let r = await pp
//     // .start(axios, { url: "http://null" })
//     .input(
//       `'<!doctype html><html itemscope="" itemtype="http://schema.org/WebPage" lang="fr"><head><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"><meta content="/images/branding/googleg/1x/googleg_standard_color_128dp.png" itemprop="image"><title>Google</title><script nonce="JQLGwY6K7yOSCYpDJ98RUQ==">(function(){window.google={kEI:\'Kxx8WtDPEoLsUvTWvNAG\',kEXPI:\'201821,1353810,1354277,1354690,1354915,1355675,1355761,1355793,1356040,1356470,1356690,1356779,1357034,1357208,1357219,1357403,1357420,1357487,1357554,1357749,3700338,3700521,4029815,4031109,4043492,4045841,4048347,4081038,4097153,4097469,4097922,4097929,4098721,4098728,4098752,4102238,4105786,4109316,4109489,4116279,4116349,4116551,4116724,4116731,4116926,4116935,4117980,4118798,4119049,4120660,4122511,4124850,4125837,4126203,4126730,4126754,4127086,4127307,4127744,4128586,4129520,4129633,4130782,4131247,4131834,4133876,4134393,4135025,4136073,4136092,4136521,4137467,4137597,4137646,4139395,4139722,4139730,4140277,4140323,4141160,4141174,4141390,4142071,4142328,4142678,4142834,4143278,4143816,4143970,4144325,4144442,4144545,4144759,4144803,4145088,4145109,4145461,4145485,4145622,4145772,4145836,4146146,4146880,4147033,4147869,4147943,4147951,4148061,4148268,4148279,4148304,4148607,4148988,4149055,4149304,4149336,4149372,4149446,4149493,4149797,4149803,4149944,4150005,4150017,4150185,4150526,4150565,4151012,4151015,4151047,4151296,4151385,4151389,4151406,4151676,4151678,4151699,4151781,4151950,4151958,4151962,4152527,4152604,4152890,4153222,4153273,4153301,10200084,10202524,10202562,10202570,15807764,19000288,19000423,19000427,19001999,19002287,19002288,19002366,19002548,19002880,19003321,19003323,19003325,19003326,19003328,19003329,19003330,19003407,19003408,19003409,19004309,19004516,19004517,19004518,19004519,19004520,19004521,19004786,19004787,19004788,19004789,19004790,19004847,19004862,19004874\',authuser:0,kscs:\'c9c918f0_Kxx8WtDPEoLsUvTWvNAG\',u:\'c9c918f0\',kGL:\'FR\'};google.kHL=\'fr\';})();(function(){google.lc=[];google.li=0;google.getEI=function(a){for(var b;a&&(!a.getAttribute||!(b=a.getAttribute("eid")));)a=a.parentNode;return b||google.kEI};google.getLEI=function(a)`
//     )
//     .next(function getdata(res) {
//       let data = get(res, "data")
//       if (data) return data
//       throw "No data parameter!"
//     })
//     .next(function getFirst10chars(data) {
//       return data.slice(0, 10)
//     })
//     // .sandbox(data => {
//     //   console.log(data)
//     // })
//     // .log()
//     // .sandbox(() => {
//     //   console.dir(pp.stack(), { depth: 4 })
//     // })
//     // .error((err, functionName) => {
//     //   console.error("Error:", err, functionName)
//     // })
//     .value()
//   console.log("Final:", r)
// })()
