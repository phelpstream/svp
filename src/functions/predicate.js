export default function predicate(fn) {
  return function(...args) {
    return !!fn(...args)
  }
}
