"use strict"
import get from './get'
import predicate from './predicate'

export default predicate(get)