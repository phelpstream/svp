export default function promisify(fn) {
  return async function promisifiedFn(...args) {
    try {
      return Promise.resolve(await fn(...args)) // eslint-disable-line no-undef
    } catch (error) {
      return Promise.reject(error) // eslint-disable-line no-undef
    }
  }
}
