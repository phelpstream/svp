import slice from "./slice"
import { isString, isArray } from "./is"
export default function split(value, splitter, limit) {
  if (isString(value)) return value.split(splitter, limit)
  if (isArray(value)) {
    let indexes = []
    let splitArray = []
    let foundIndex = 0
    while ((foundIndex = value.indexOf(splitter, foundIndex + 1)) !== -1) {
      indexes.push(foundIndex)
    }
    if (limit) {
      indexes = slice(indexes, 0, limit)
    }
    let previousIndex = -1
    for (let index of indexes) {
      splitArray.push(value.slice(previousIndex + 1, index))
      previousIndex = index
    }
    splitArray.push(value.slice(previousIndex + 1, value.length))
    return splitArray
  }
  return null
}

// console.log(split("Bonjour", "waoo"))
// console.log(split(["Hello", "@", "Gabin", "!", "@", "Super"], "waoo"))
