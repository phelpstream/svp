import sortArr from "./sortArr"

export default function sortEntries(entries, sortFn) {
  let sortByKey = (entry1, entry2) => {
    let [key1, value1] = entry1 // eslint-disable-line no-unused-vars
    let [key2, value2] = entry2 // eslint-disable-line no-unused-vars
    if (key1 < key2) return -1
    if (key1 > key2) return 1
    return 0
  }
  sortFn = sortFn || sortByKey
  return sortArr(entries, sortFn)
}
