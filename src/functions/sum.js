import operation from "./operation"

export default function sum(nb, ...nbs) {
  return operation((total, n) => total + n, nb, ...nbs)
}

// export default function sum(inc, nb = 0, ...nbs) {
//   if (nbs.length == 0) return inc + nb
//   return sum(inc + nb, ...nbs)
// }

// console.log(sum(1,2,3,4))
