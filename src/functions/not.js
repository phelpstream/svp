export default function not(fn) {
  return function negated(...args) {
    return !fn(...args)
  }
}
