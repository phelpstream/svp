import { isDefined } from "./is"

// If Then Return
export default function iftr(conditionResult, trueValue, falseValue) {
  if (conditionResult && isDefined(trueValue)) return trueValue
  if (!conditionResult && isDefined(falseValue)) return falseValue
}
