import { expect } from "chai"

import curry from "./curry"

describe("curry.js", () => {
  it("should curry a function by allowing to call several times with extra parameters", () => {
    let sumFourNb = (a, b, c, d) => a + b + c + d
    let sumFourNbCurried = curry(sumFourNb)
    expect(sumFourNbCurried(1)(2, 3)(4)).to.be.equal(10)
  })
})
