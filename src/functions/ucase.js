"use strict"
export default function ucase(value) {
  return value.toUpperCase()
}
