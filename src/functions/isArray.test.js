import { expect } from "chai"
import VARS from "./../consts/tests.vars"
import isArray from "./isArray"

const testName = "ARRAY"

describe("isArray.js", () => {
  // NUMBERS
  it("should find that a(n) [NUMBER.INFINITY] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.NUMBER.INFINITY)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.INFINITY_CALC] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.NUMBER.INFINITY_CALC)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.MINUS_INFINITY_CALC] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.NUMBER.MINUS_INFINITY_CALC)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.INTEGER] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.NUMBER.INTEGER)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.FLOAT] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.NUMBER.FLOAT)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.LONG_FLOAT] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.NUMBER.LONG_FLOAT)
    expect(result).to.be.false
  })

  // STRINGS
  it("should find that a(n) [STRING.EMPTY] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.STRING.EMPTY)
    expect(result).to.be.false
  })
  it("should find that a(n) [STRING.SIMPLE] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.STRING.SIMPLE)
    expect(result).to.be.false
  })

  // OBJECTS
  it("should find that a(n) [OBJECT.EMPTY] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.OBJECT.EMPTY)
    expect(result).to.be.false
  })
  it("should find that a(n) [OBJECT.SIMPLE] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.OBJECT.SIMPLE)
    expect(result).to.be.false
  })

  // BOOLEANS
  it("should find that a(n) [BOOLEAN.TRUE] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.BOOLEAN.TRUE)
    expect(result).to.be.false
  })
  it("should find that a(n) [BOOLEAN.FALSE] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.BOOLEAN.FALSE)
    expect(result).to.be.false
  })

  // ARRAYS
  it("should find that a(n) [ARRAY.EMPTY] value [IS] an [" + testName + "]", () => {
    let result = isArray(VARS.ARRAY.EMPTY)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.STRING] value [IS] an [" + testName + "]", () => {
    let result = isArray(VARS.ARRAY.STRING)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.INTEGER] value [IS] an [" + testName + "]", () => {
    let result = isArray(VARS.ARRAY.INTEGER)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.UNDEFINED] value [IS] an [" + testName + "]", () => {
    let result = isArray(VARS.ARRAY.UNDEFINED)
    expect(result).to.be.true
  })

  // SETS
  it("should find that a(n) [SET.INSTANCE] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.SET.INSTANCE)
    expect(result).to.be.false
  })
  it("should find that a(n) [SET.SIMPLE] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.SET.SIMPLE)
    expect(result).to.be.false
  })

  // WEAKSETS
  it("should find that a(n) [WEAKSET.INSTANCE] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.WEAKSET.INSTANCE)
    expect(result).to.be.false
  })
  it("should find that a(n) [WEAKSET.SIMPLE] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.WEAKSET.SIMPLE)
    expect(result).to.be.false
  })

  // MAPS
  it("should find that a(n) [MAP.INSTANCE] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.MAP.INSTANCE)
    expect(result).to.be.false
  })
  it("should find that a(n) [MAP.SIMPLE] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.MAP.SIMPLE)
    expect(result).to.be.false
  })

  // WEAKMAPS
  it("should find that a(n) [WEAKMAP.INSTANCE] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.WEAKMAP.INSTANCE)
    expect(result).to.be.false
  })
  it("should find that a(n) [WEAKMAP.SIMPLE] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.WEAKMAP.SIMPLE)
    expect(result).to.be.false
  })

  // SPECIAL CASES
  it("should find that a(n) [NOT INSTANCIATED] value [IS NOT] an [" + testName + "]", () => {
    let value
    let result = isArray(value)
    expect(result).to.be.false
  })
  it("should find that a(n) [UNDEFINED] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.UNEFINED)
    expect(result).to.be.false
  })
  it("should find that a(n) [NULL] value [IS NOT] an [" + testName + "]", () => {
    let result = isArray(VARS.NULL)
    expect(result).to.be.false
  })
})
