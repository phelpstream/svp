import { expect } from "chai"

import baseSort from "./baseSort"

describe("baseSort.js", () => {
  it("should sort based on numbers", () => {
    expect(baseSort([3, 2, 1])).to.be.eql([1, 2, 3])
  })
  it("should sort based on subset number in array", () => {
    expect(baseSort([[3, "three"], [2, "two"], [1, "one"]], a => a, 0)).to.be.eql([[1, "one"], [2, "two"], [3, "three"]])
  })
})
