export default function iteratingItem(itValue, itKey, itPath) {
  if (!(this instanceof iteratingItem)) return new iteratingItem(itValue, itKey, itPath)
  let value = itValue
  let key = itKey
  let path = itPath

  let newValue = itValue
  let newValueHasBeenSet = false
  let nextValueToIterateOver = itValue
  let removeMe = false

  this.value = function() {
    return value
  }

  this.key = function() {
    return key
  }

  this.path = function() {
    return path
  }

  this.status = function() {
    return {
      value: newValue,
      valueSet: newValueHasBeenSet,
      next: nextValueToIterateOver,
      remove: removeMe
    }
  }

  this.remove = function() {
    removeMe = true
    return this
  }

  this.keep = function() {
    removeMe = false
    return this
  }

  this.next = function(newNextValueToIterate = itValue) {
    nextValueToIterateOver = newNextValueToIterate
    return this
  }

  this.set = function(newValueToSet) {
    newValueHasBeenSet = true
    newValue = newValueToSet
    return this
  }

  this.newValue = function() {
    return newValue
  }

  return this
}
