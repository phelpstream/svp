import { expect } from "chai"

import normalizeObj from "./normalizeObj"

describe("normalizeObj.js", () => {
  it("should normalize an object", () => {
    let obj = { bonjour: true, allo: false }
    let normalizedObj = normalizeObj(obj)
    expect(normalizedObj).to.be.eql({ allo: false, bonjour: true })
  })
})
