export default function slice(value = [], ...args) {
  return value.slice(...args)
}
