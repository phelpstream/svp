/* eslint-disable */

import get from "./get"
import has from "./has"

// TODO: MAKE IT ALL LEVELS COMPATIBLE :D

export default function pick(obj, paths = []) {
  return paths.reduce((pickedObj, path) => {
    if (has(obj, path)) pickedObj[path] = obj[path]
    return pickedObj
  }, {})
}

// tests

// let o = {
//   a: 1,
//   b: 2,
//   c: 3
// }

// console.dir(pick(o, ["a", "c"]))
