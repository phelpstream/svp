/**
 * Deduplicates an array
 * @param {*} value The array to deduplicate.
 */
export default function dedupeLight(value) {
  return Array.from(new Set(value)) // eslint-disable-line no-undef
}
