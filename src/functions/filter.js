import dig from "./dig"
import isIterable from "./isIterable"

export default function filter(filterFn, obj) {
  return dig(
    obj,
    v => v,
    (item, val, key, path) => {
      if (filterFn(val, key, path)) {
        if (isIterable(val)) return item.next(val)
        return item.set(val)
      }
      return item.remove()
    }
  )
}

// tests

// import isEmpty from "./isEmpty"
// import not from "./not"

// let o = { amazing: [{ ok: ["wahou"], other: "", another: [] }], ok: true }
// let a = [1, 2, [2, "", [[[]]]], "", { ok: ["som"], else: "ok" }, {}]

// console.log(filter(not(isEmpty), a))
