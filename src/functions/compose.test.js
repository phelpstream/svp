import { expect } from "chai"

import compose from "./compose"

describe("compose.js", () => {
  it("compose functions", () => {
    let suffix = value => value + "_suffix"
    let ucase = value => value.toUpperCase()
    expect(compose(ucase, suffix)("word")).to.be.equal("WORD_SUFFIX")
  })
})
