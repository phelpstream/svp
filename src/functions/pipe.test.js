import { expect } from "chai"

import pipe from "./pipe"

describe("pipe.js", () => {
  it("pipe functions", () => {
    let suffix = value => value + "_suffix"
    let ucase = value => value.toUpperCase()
    expect(pipe(suffix, ucase)("word")).to.be.equal('WORD_SUFFIX')
  })
})