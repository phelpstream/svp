import { expect } from "chai"

import toEntries from "./toEntries"

describe("toEntries.js", () => {
  it("should get the entries for an array", () => {
    let value = [1, 2, 3]
    let entries = toEntries(value)
    expect(entries).to.be.eql([["0", 1], ["1", 2], ["2", 3]])
  })
})
