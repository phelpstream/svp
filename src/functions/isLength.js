import length from "./length"

export default function isLength(value, testLengthNb) {
  return length(value) === testLengthNb
}
