"use strict"
export default function freeze(obj) {
  return Object.freeze(obj)
}
