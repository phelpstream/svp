import { expect } from "chai"

import flip from "./flip"

describe("flip.js", () => {
  it("changes a function by flipping it's parameters", () => {
    let add1 = n => n + 1
    let add2 = flip(add1)
    let order1 = [1, 2, 3]
    let order2 = [3, 2, 1]
    expect(add1(...order1)).to.be.eql(add2(...order2))
  })
})
