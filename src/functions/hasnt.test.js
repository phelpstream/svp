"use strict"
import { expect } from "chai"

import hasnt from "./hasnt"

describe("hasnt.js", () => {
  it("should check if a 1 level property isn't in an object", () => {
    let obj = {
      first: "yes"
    }
    expect(hasnt(obj, "first")).to.be.false
  })
  it("should check if a 3 levels property isn't in an object", () => {
    let obj = {
      first: {
        second: [
          "one",
          {
            third: {
              ok: "cool"
            }
          }
        ]
      }
    }

    expect(hasnt(obj, "first.second.1.third.ok")).to.be.false
  })
})
