import { isObject, isNumber } from "./is"

export default function toArray(value) {
  if (isObject(value) && Object.entries(value).every((v, key) => isNumber(+key))) {
    return Object.entries(value)
      .sort(([k1], [k2]) => +k1 - +k2)
      .map(([k, v]) => v) // eslint-disable-line no-unused-vars
      .reduce((redArr, redVal, redKey) => { // eslint-disable-line no-unused-vars
        redArr.push(redVal)
        return redArr
      }, [])
  }
  return Array.from(value)
}
