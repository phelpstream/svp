import { expect } from "chai"

import baseToString from "./baseToString"

describe("baseToString.js", () => {
  it("should return a string from a string", () => {
    expect(baseToString("a string")).to.be.equal("a string")
  })
  it("should return a string from an array of strings", () => {
    expect(baseToString(["an", "array", "of", "strings"])).to.be.equal("an array of strings")
  })
  it("should return a string from a number", () => {
    expect(baseToString(1234)).to.be.equal("1234")
  })
  it("should return a string from an object", () => {
    expect(baseToString({ hello: "you" })).to.be.equal("{\"hello\":\"you\"}")
  })
})
