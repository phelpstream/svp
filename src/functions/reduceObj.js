import reduceArr from "./reduceArr"
import toEntries from "./toEntries"

export default function reduceObj(obj, reducerFn, initialObj = {}, entriesModifierFn = v => v, sortFn) {
  let _entries = toEntries(obj)
  _entries = entriesModifierFn(_entries, sortFn)
  return reduceArr(
    _entries,
    function(finalObj, [key, value], index) {
      reducerFn(finalObj, value, key, index)
      return finalObj
    },
    initialObj
  )
}

