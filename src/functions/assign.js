"use strict"
export default function assign(targetObject, ...sourceObjects) {
  return Object.assign(targetObject, ...sourceObjects)
}
