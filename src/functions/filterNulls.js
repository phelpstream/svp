import filter from "./filter"
import isNotNull from "./isNotNull"

export default function filterNulls(value) {
  return filter(v => isNotNull(v), value)
}

// console.log(filterNulls({ amazing: [{ ok: [] }], ok: null }))
