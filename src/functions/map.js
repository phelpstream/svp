export default function map(value, iteratee) {
  return value.map(iteratee)
}
