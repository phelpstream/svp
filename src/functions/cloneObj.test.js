import { expect } from "chai"

import cloneObj from "./cloneObj"

describe("cloneObj.js", () => {
  it("should clone an object (no possible mutations)", () => {
    let obj1 = { a: 1 }
    let obj2 = cloneObj(obj1)
    obj1.a = 2
    expect(obj2).to.not.contain({ a: 2 })
  })
})
