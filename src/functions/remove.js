import { isArray, isObject, isString } from "./is"
import removeFromArray from "./removeFromArray"
import removeFromObject from "./removeFromObject"
import removeFromString from "./removeFromString"

export default function remove(value, index, mutable = false) {
  if (isArray(value)) removeFromArray(value, index, mutable)
  if (isObject(value)) removeFromObject(value, index, mutable)
  if (isString(value)) removeFromString(value, index, mutable)
  return value
}
