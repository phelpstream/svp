import { expect } from "chai"

import freeze from "./freeze"

describe("freeze.js", () => {
  it("freeze an object  - get it immutable", () => {
    let obj = { one: true, two: "ok" }
    let newObj = freeze(obj)
    expect(obj).to.be.eql(newObj)
  })
})
