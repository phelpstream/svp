"use strict"
import pipe from "./pipe"
import flip from "./flip"

export default flip(pipe)

