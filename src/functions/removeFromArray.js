import clone from "./clone"
import splice from "./splice"

export default function removeFromArray(value, index, mutable = false) {
  if (mutable) return splice(value, index, 1) && value
  return splice(clone(value), index, 1)
}
