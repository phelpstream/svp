import every from "./every"

export default function and(...args) {
  return every(args, testResult => testResult === true)
}
