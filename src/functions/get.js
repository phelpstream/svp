"use strict"
import parsePath from "./parsePath"
import reduceArr from "./reduceArr"
import cloneObj from "./cloneObj"
import clone from "./clone"
import baseGet from "./baseGet"
import isNull from "./isNull"

export default function get(obj, path, defaultValue = undefined, mutable = false) {
  let [_path, _obj] = [parsePath(path, obj), cloneObj(obj)]
  let resultValue = reduceArr(_path, baseGet, _obj)
  return isNull(resultValue) ? (defaultValue && !mutable ? clone(defaultValue) : defaultValue) : mutable ? resultValue : clone(resultValue)
}

// let people = {
//   "1234": { firstName: "Chris", lastName: "Smith" },
//   "5678": { firstName: "James", lastName: "Brown" }
// }

// let person = get(people, "1234")
// person.firstName = "OhNo"

// console.log(people)
