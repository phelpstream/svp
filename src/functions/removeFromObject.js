import clone from "./clone"

export default function removeFromObject(value, index, mutable = false) {
  if (mutable) return delete value[index] && value
  else {
    let _value = clone(value)
    return delete _value[index] && _value
  }
}

// console.log(removeFromObject({ ok: true, no: false }, "no"))
