import { expect } from "chai"
import VARS from "./../consts/tests.vars"
import isSameType from "./isSameType"

const testName = "SAME TYPE"

describe("isSameType.js", () => {
  // NUMBERS
  it("should find that a(n) [NUMBER.INFINITY] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.NUMBER.INFINITY, VARS.NUMBER.INFINITY, VARS.NUMBER.INFINITY)
    expect(result).to.be.true
  })
  it("should find that a(n) [NUMBER.INFINITY_CALC] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.NUMBER.INFINITY_CALC, VARS.NUMBER.INFINITY_CALC, VARS.NUMBER.INFINITY_CALC)
    expect(result).to.be.true
  })
  it("should find that a(n) [NUMBER.MINUS_INFINITY_CALC] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.NUMBER.MINUS_INFINITY_CALC, VARS.NUMBER.MINUS_INFINITY_CALC, VARS.NUMBER.MINUS_INFINITY_CALC)
    expect(result).to.be.true
  })
  it("should find that a(n) [NUMBER.INTEGER] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.NUMBER.INTEGER, VARS.NUMBER.INTEGER, VARS.NUMBER.INTEGER)
    expect(result).to.be.true
  })
  it("should find that a(n) [NUMBER.FLOAT] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.NUMBER.FLOAT, VARS.NUMBER.FLOAT, VARS.NUMBER.FLOAT)
    expect(result).to.be.true
  })
  it("should find that a(n) [NUMBER.LONG_FLOAT] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.NUMBER.LONG_FLOAT, VARS.NUMBER.LONG_FLOAT, VARS.NUMBER.LONG_FLOAT)
    expect(result).to.be.true
  })

  // STRINGS
  // it("should find that a(n) [STRING.EMPTY] value [IS] (an) [" + testName + "]", () => {
  //   let result = isSameType(VARS.STRING.EMPTY,VARS.STRING.EMPTY,VARS.STRING.EMPTY)
  //   expect(result).to.be.true
  // })
  it("should find that a(n) [STRING.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.STRING.SIMPLE, VARS.STRING.SIMPLE, VARS.STRING.SIMPLE)
    expect(result).to.be.true
  })

  // OBJECTS
  it("should find that a(n) [OBJECT.EMPTY] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.OBJECT.EMPTY, VARS.OBJECT.EMPTY, VARS.OBJECT.EMPTY)
    expect(result).to.be.true
  })
  it("should find that a(n) [OBJECT.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.OBJECT.SIMPLE, VARS.OBJECT.SIMPLE, VARS.OBJECT.SIMPLE)
    expect(result).to.be.true
  })

  // BOOLEANS
  it("should find that a(n) [BOOLEAN.TRUE] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.BOOLEAN.TRUE, VARS.BOOLEAN.TRUE, VARS.BOOLEAN.TRUE)
    expect(result).to.be.true
  })
  // it("should find that a(n) [BOOLEAN.FALSE] value [IS] (an) [" + testName + "]", () => {
  //   let result = isSameType(VARS.BOOLEAN.FALSE, VARS.BOOLEAN.FALSE, VARS.BOOLEAN.FALSE)
  //   expect(result).to.be.true
  // })

  // ARRAYS
  it("should find that a(n) [ARRAY.EMPTY] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.ARRAY.EMPTY, VARS.ARRAY.EMPTY, VARS.ARRAY.EMPTY)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.STRING] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.ARRAY.STRING, VARS.ARRAY.STRING, VARS.ARRAY.STRING)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.INTEGER] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.ARRAY.INTEGER, VARS.ARRAY.INTEGER, VARS.ARRAY.INTEGER)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.UNDEFINED] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.ARRAY.UNDEFINED, VARS.ARRAY.UNDEFINED, VARS.ARRAY.UNDEFINED)
    expect(result).to.be.true
  })

  // SETS
  it("should find that a(n) [SET.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.SET.INSTANCE, VARS.SET.INSTANCE, VARS.SET.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [SET.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.SET.SIMPLE, VARS.SET.SIMPLE, VARS.SET.SIMPLE)
    expect(result).to.be.true
  })

  // WEAKSETS
  it("should find that a(n) [WEAKSET.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.WEAKSET.INSTANCE, VARS.WEAKSET.INSTANCE, VARS.WEAKSET.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [WEAKSET.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.WEAKSET.SIMPLE, VARS.WEAKSET.SIMPLE, VARS.WEAKSET.SIMPLE)
    expect(result).to.be.true
  })

  // MAPS
  it("should find that a(n) [MAP.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.MAP.INSTANCE, VARS.MAP.INSTANCE, VARS.MAP.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [MAP.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.MAP.SIMPLE, VARS.MAP.SIMPLE, VARS.MAP.SIMPLE)
    expect(result).to.be.true
  })

  // WEAKMAPS
  it("should find that a(n) [WEAKMAP.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.WEAKMAP.INSTANCE, VARS.WEAKMAP.INSTANCE, VARS.WEAKMAP.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [WEAKMAP.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isSameType(VARS.WEAKMAP.SIMPLE, VARS.WEAKMAP.SIMPLE, VARS.WEAKMAP.SIMPLE)
    expect(result).to.be.true
  })

  // SPECIAL CASES
  // it("should find that a(n) [NOT INSTANCIATED] value [IS] (an) [" + testName + "]", () => {
  //   let value
  //   let result = isSameType(value, value, value)
  //   expect(result).to.be.true
  // })
  // it("should find that a(n) [UNDEFINED] value [IS] (an) [" + testName + "]", () => {
  //   let result = isSameType(VARS.UNEFINED, VARS.UNEFINED, VARS.UNEFINED)
  //   expect(result).to.be.true
  // })
  // it("should find that a(n) [NULL] value [IS] (an) [" + testName + "]", () => {
  //   let result = isSameType(VARS.NULL, VARS.NULL, VARS.NULL)
  //   expect(result).to.be.true
  // })
})
