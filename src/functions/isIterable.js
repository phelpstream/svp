import { isNull, isObject, isNumber, isString, isFunction } from "./is"

/**
 *
 * @param {*} value The value to check if iterable.
 * @param {Boolean} includeObjects Include objects.
 */
export default function isIterable(value, { objects = false, numbers = false, strings = false } = {}) {
  if (isNull(value)) return false
  return (
    (objects && isObject(value)) ||
    (numbers && isNumber(value)) ||
    (strings && isString(value) && isFunction(value[Symbol.iterator])) || // eslint-disable-line no-undef
    (isFunction(value[Symbol.iterator]) && !isString(value)) // eslint-disable-line no-undef
  )
}
