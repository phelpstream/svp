export default function splice(arr, startIndex, itemsNbToDelete, ...valuesToInject) {
  return arr.splice(startIndex, itemsNbToDelete, ...valuesToInject) && arr
}
