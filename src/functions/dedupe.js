import normalize from "./normalize"
import { isObject } from "./is"
import stringify from "./stringify"

export default function dedupe(arr) {
  let _simplifiedArr = arr.map(a => normalize(a)).map(a => (isObject(a) ? stringify(a) : a))
  let _dedupedArr = Array.from(new Set(_simplifiedArr)) // eslint-disable-line no-undef
  let _filteredIndexes = _dedupedArr.map(d => _simplifiedArr.indexOf(d))
  return _filteredIndexes.map(i => arr[i])
}

// let arr = ["bonjour", "coucou", "ok", "bonjour"]
// let arr = [
//   { something: "saying hi", ok: ["deux", "ah"] },
//   { something: "saying hoi" },
//   { something: "saying hi", ok: ["deux", "ah"] },
//   { something: "saying hia" }
// ]
// console.log(dedupe(arr))
