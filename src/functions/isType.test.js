import { expect } from "chai"

import isType from "./isType"

describe("isType.js", () => {
  it("should detect a string type", () => {
    expect(isType("string", "a string")).to.be.true
  })
  it("should detect a number type", () => {
    expect(isType("number", 123)).to.be.true
  })
  it("should detect a boolean type", () => {
    expect(isType("boolean", true)).to.be.true
  })
  it("should detect an object type", () => {
    expect(isType("object", {})).to.be.true
  })
})
