export default function curry(fn, length) {
  length = length || fn.length
  return function currified(...args) {
    if (args.length === 0) return currified
    if (args.length >= length) return fn.apply(this, args)
    const child = fn.bind.apply(fn, [this].concat(args))
    return curry(child, length - args.length)
  }
}
