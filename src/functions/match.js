export default function match(string, regex) {
  return !!(string.match(regex) && string.match(regex).length > 0)
}
