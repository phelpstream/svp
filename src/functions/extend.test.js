import { expect } from "chai"

import extend from "./extend"

describe("extend.js", () => {
  it("should extend properties of an object", () => {
    let obj1 = { a: 1 }
    let obj2 = { b: 2 }
    let extendedObj = extend(obj1, obj2)
    expect(extendedObj).to.contain({ a: 1, b: 2 })
  })
  it("should not alter properties of the source object", () => {
    let obj1 = { a: 1 }
    let obj2 = { b: 2 }
    let extendedObj = extend(obj1, obj2) // eslint-disable-line no-unused-vars
    expect(obj1).to.not.contain({ b: 2 })
  })
  it("should not be linked to the sources objects", () => {
    let obj1 = { a: 1 }
    let obj2 = { b: 2 }
    let extendedObj = extend(obj1, obj2)
    obj2.c = 3
    expect(extendedObj).to.not.contain({ c: 3 })
  })
  it("should be linked to target object", () => {
    let obj1 = { a: 1 }
    let obj2 = { b: 2 }
    let extendedObj = extend(obj1, obj2)
    obj1.c = 3
    expect(extendedObj).to.contain({ c: 3 })
  })
})
