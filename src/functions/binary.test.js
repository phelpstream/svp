import { expect } from "chai"

import binary from './binary'

describe('binary.js', () => {
  it('should convert a 2 arguments function to a one argument one', () => {
    let fn = (a, b, c) => a // eslint-disable-line no-unused-vars
    let binaryFn = binary(fn)
    expect(binaryFn(1)).to.be.equal(fn(1))
  });
});
