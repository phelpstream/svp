import typeOf from "./typeOf"

export default function isType(type, value) {
  return typeOf(value) === type
}
