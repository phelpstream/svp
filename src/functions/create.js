export default function create(objectPrototype, objProperties = undefined) {
  return Object.create(objectPrototype, objProperties)
}
