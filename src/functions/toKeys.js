import { isObject, isString, isNumber, isIterable, isArray } from "./is"
import toArray from "./toArray"

/**
 *
 * @param {*} value The value to convert to entries (key, val)
 */
export default function toKeys(value) {
  if (isArray(value)) return Object.keys(value)
  if (isObject(value)) return toArray(Object.keys(value))
  if (isString(value) || isNumber(value)) return toArray(toArray(`${value}`).keys())
  if (isIterable(value)) return toArray(value.keys())
  return []
}
