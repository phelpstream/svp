import isObject from "./isObject"
import isNumber from "./isNumber"

export default function isKey(key, obj) {
  if (isObject(key) || isNumber(obj)) {
    console.error("/!\\ WARNING! The function isKey(key, obj) has now the key as first argument, careful! [ key", key, "obj", obj, "]")
    return !!(key && key.hasOwnProperty(obj)) || false // temp fix
  }
  return (obj && obj.hasOwnProperty(key)) || false
}
