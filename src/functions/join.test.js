import { expect } from "chai"

import join from "./join"

describe('join.js', () => {
  it('should join several strings with a delimiter', () => {
    let arr = ["an", "array", "of", "strings"]
    expect(join(arr, " ")).to.be.equal("an array of strings")
  });
});
