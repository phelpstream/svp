import { expect } from "chai"
import VARS from "./../consts/tests.vars"
import isEven from "./isEven"

const testName = "EVEN NUMBER"

describe("isEven.js", () => {
  // NUMBERS
  it("should find that a(n) [NUMBER.EVEN] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.NUMBER.EVEN)
    expect(result).to.be.true
  })
  it("should find that a(n) [NUMBER.ODD] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isEven(VARS.NUMBER.ODD)
    expect(result).to.be.false
  })
  // it("should find that a(n) [NUMBER.INFINITY] value [IS] (an) [" + testName + "]", () => {
  //   let result = isEven(VARS.NUMBER.INFINITY)
  //   expect(result).to.be.true
  // })
  // it("should find that a(n) [NUMBER.INFINITY_CALC] value [IS] (an) [" + testName + "]", () => {
  //   let result = isEven(VARS.NUMBER.INFINITY_CALC)
  //   expect(result).to.be.true
  // })
  // it("should find that a(n) [NUMBER.MINUS_INFINITY_CALC] value [IS] (an) [" + testName + "]", () => {
  //   let result = isEven(VARS.NUMBER.MINUS_INFINITY_CALC)
  //   expect(result).to.be.true
  // })
  // it("should find that a(n) [NUMBER.INTEGER] value [IS] (an) [" + testName + "]", () => {
  //   let result = isEven(VARS.NUMBER.INTEGER)
  //   expect(result).to.be.true
  // })
  // it("should find that a(n) [NUMBER.FLOAT] value [IS] (an) [" + testName + "]", () => {
  //   let result = isEven(VARS.NUMBER.FLOAT)
  //   expect(result).to.be.true
  // })
  // it("should find that a(n) [NUMBER.LONG_FLOAT] value [IS] (an) [" + testName + "]", () => {
  //   let result = isEven(VARS.NUMBER.LONG_FLOAT)
  //   expect(result).to.be.true
  // })

  // STRINGS
  it("should find that a(n) [STRING.EMPTY] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.STRING.EMPTY)
    expect(result).to.be.true
  })
  it("should find that a(n) [STRING.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.STRING.SIMPLE)
    expect(result).to.be.true
  })

  // OBJECTS
  it("should find that a(n) [OBJECT.EMPTY] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.OBJECT.EMPTY)
    expect(result).to.be.true
  })
  it("should find that a(n) [OBJECT.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.OBJECT.SIMPLE)
    expect(result).to.be.true
  })

  // BOOLEANS
  it("should find that a(n) [BOOLEAN.TRUE] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.BOOLEAN.TRUE)
    expect(result).to.be.true
  })
  it("should find that a(n) [BOOLEAN.FALSE] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.BOOLEAN.FALSE)
    expect(result).to.be.true
  })

  // ARRAYS
  it("should find that a(n) [ARRAY.EMPTY] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.ARRAY.EMPTY)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.STRING] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.ARRAY.STRING)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.INTEGER] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.ARRAY.INTEGER)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.UNDEFINED] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.ARRAY.UNDEFINED)
    expect(result).to.be.true
  })

  // SETS
  it("should find that a(n) [SET.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.SET.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [SET.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.SET.SIMPLE)
    expect(result).to.be.true
  })

  // WEAKSETS
  it("should find that a(n) [WEAKSET.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.WEAKSET.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [WEAKSET.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.WEAKSET.SIMPLE)
    expect(result).to.be.true
  })

  // MAPS
  it("should find that a(n) [MAP.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.MAP.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [MAP.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.MAP.SIMPLE)
    expect(result).to.be.true
  })

  // WEAKMAPS
  it("should find that a(n) [WEAKMAP.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.WEAKMAP.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [WEAKMAP.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isEven(VARS.WEAKMAP.SIMPLE)
    expect(result).to.be.true
  })

  // SPECIAL CASES
  // it("should find that a(n) [NOT INSTANCIATED] value [IS NOT] (an) [" + testName + "]", () => {
  //   let value
  //   let result = isEven(value)
  //   expect(result).to.be.false
  // })
  // it("should find that a(n) [UNDEFINED] value [IS NOT] (an) [" + testName + "]", () => {
  //   let result = isEven(VARS.UNEFINED)
  //   expect(result).to.be.false
  // })
  // it("should find that a(n) [NULL] value [IS NOT] (an) [" + testName + "]", () => {
  //   let result = isEven(VARS.NULL)
  //   expect(result).to.be.true
  // })
})
