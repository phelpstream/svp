import { expect } from "chai"

import cloneArr from "./cloneArr"

describe("cloneArr.js", () => {
  it("should clone an array (no possible mutations)", () => {
    let arr1 = [1, 2, 3]
    let arr2 = cloneArr(arr1)
    arr1.push(4)
    expect(arr2).to.not.contain(4)
  })
})
