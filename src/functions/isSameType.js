export default function isSameType(argRef, ...args) {
  return argRef && args.every(arg => typeof argRef === typeof arg)
}
