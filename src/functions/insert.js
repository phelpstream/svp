export default function insert(str, strToInsert, index) {
  return str.slice(0, index) + strToInsert + str.slice(index)
}

// let w = "Coucou"
// console.log(insert(w, "ok", 3))
