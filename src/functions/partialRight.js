export default function partialRight(fn, arg1) {
  return function(...args) {
    return fn(...args, arg1)
  }
}
