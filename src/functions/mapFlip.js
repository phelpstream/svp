import map from "./map"
import reverse from "./reverse"

export default function mapFlip(arr, ...args) {
  return map(reverse(arr), ...args)
}
