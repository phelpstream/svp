import { isObject, isArray } from "./is"
import sortObj from "./sortObj"
import sortArr from "./sortArr"

export default function sort(value, sortFn) {
  if (isObject(value)) return sortObj(value, sortFn)
  if (isArray(value)) return sortArr(value, sortFn)
  return value
}
