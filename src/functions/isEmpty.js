import { isString, isArray, isObject } from "./is"
import isEmptyString from "./isEmptyString"
import isEmptyArray from "./isEmptyArray"
import isEmptyObject from "./isEmptyObject"

export default function isEmpty(value) {
  if (isArray(value)) return isEmptyArray(value)
  if (isObject(value)) return isEmptyObject(value)
  if (isString(value)) return isEmptyString(value)
  return null
}

// console.log(isEmpty([]));
// console.log(isString(""));
// console.log("isEmpty", isEmpty(""))
// console.log("isEmptyString", isEmptyString(""))
// console.log(isEmpty({}));
