import { expect } from "chai"

import unshift from "./unshift"

describe("unshift.js", () => {
  it("should add values at the beginning of an array", () => {
    let arr = [1, 2, 3]
    expect(unshift(arr, 4, 5)).to.be.eql([4, 5, 1, 2, 3])
  })
})
