import { expect } from "chai"

import cloneStr from "./cloneStr"

describe("cloneStr.js", () => {
  it("should clone a string (no possible mutations)", () => {
    let str1 = "a string"
    let str2 = cloneStr(str1)
    str1 = "another string"
    expect(str2).to.be.equal("a string")
  })
})
