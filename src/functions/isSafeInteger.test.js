import { expect } from "chai"
import VARS from "./../consts/tests.vars"
import isSafeInteger from "./isSafeInteger"

const testName = "SAFEINTEGER"

describe("isSafeInteger.js", () => {
  // NUMBERS
  it("should find that a(n) [NUMBER.INFINITY] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.NUMBER.INFINITY)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.INFINITY_CALC] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.NUMBER.INFINITY_CALC)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.MINUS_INFINITY_CALC] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.NUMBER.MINUS_INFINITY_CALC)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.INTEGER] value [IS] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.NUMBER.INTEGER)
    expect(result).to.be.true
  })
  it("should find that a(n) [NUMBER.FLOAT] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.NUMBER.FLOAT)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.LONG_FLOAT] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.NUMBER.LONG_FLOAT)
    expect(result).to.be.false
  })

  // STRINGS
  it("should find that a(n) [STRING.EMPTY] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.STRING.EMPTY)
    expect(result).to.be.false
  })
  it("should find that a(n) [STRING.SIMPLE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.STRING.SIMPLE)
    expect(result).to.be.false
  })

  // OBJECTS
  it("should find that a(n) [OBJECT.EMPTY] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.OBJECT.EMPTY)
    expect(result).to.be.false
  })
  it("should find that a(n) [OBJECT.SIMPLE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.OBJECT.SIMPLE)
    expect(result).to.be.false
  })

  // BOOLEANS
  it("should find that a(n) [BOOLEAN.TRUE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.BOOLEAN.TRUE)
    expect(result).to.be.false
  })
  it("should find that a(n) [BOOLEAN.FALSE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.BOOLEAN.FALSE)
    expect(result).to.be.false
  })

  // ARRAYS
  it("should find that a(n) [ARRAY.EMPTY] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.ARRAY.EMPTY)
    expect(result).to.be.false
  })
  it("should find that a(n) [ARRAY.STRING] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.ARRAY.STRING)
    expect(result).to.be.false
  })
  it("should find that a(n) [ARRAY.INTEGER] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.ARRAY.INTEGER)
    expect(result).to.be.false
  })
  it("should find that a(n) [ARRAY.UNDEFINED] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.ARRAY.UNDEFINED)
    expect(result).to.be.false
  })

  // SETS
  it("should find that a(n) [SET.INSTANCE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.SET.INSTANCE)
    expect(result).to.be.false
  })
  it("should find that a(n) [SET.SIMPLE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.SET.SIMPLE)
    expect(result).to.be.false
  })

  // WEAKSETS
  it("should find that a(n) [WEAKSET.INSTANCE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.WEAKSET.INSTANCE)
    expect(result).to.be.false
  })
  it("should find that a(n) [WEAKSET.SIMPLE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.WEAKSET.SIMPLE)
    expect(result).to.be.false
  })

  // MAPS
  it("should find that a(n) [MAP.INSTANCE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.MAP.INSTANCE)
    expect(result).to.be.false
  })
  it("should find that a(n) [MAP.SIMPLE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.MAP.SIMPLE)
    expect(result).to.be.false
  })

  // WEAKMAPS
  it("should find that a(n) [WEAKMAP.INSTANCE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.WEAKMAP.INSTANCE)
    expect(result).to.be.false
  })
  it("should find that a(n) [WEAKMAP.SIMPLE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.WEAKMAP.SIMPLE)
    expect(result).to.be.false
  })

  // SPECIAL CASES
  it("should find that a(n) [NOT INSTANCIATED] value [IS NOT] (an) [" + testName + "]", () => {
    let value
    let result = isSafeInteger(value)
    expect(result).to.be.false
  })
  it("should find that a(n) [UNDEFINED] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.UNEFINED)
    expect(result).to.be.false
  })
  it("should find that a(n) [NULL] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isSafeInteger(VARS.NULL)
    expect(result).to.be.false
  })
})
