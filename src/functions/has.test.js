"use strict"
import { expect } from "chai"

import has from "./has"

describe("has.js", () => {
  it("should check if a 1 level property is in an object", () => {
    let obj = {
      first: "yes"
    }
    expect(has(obj, "first")).to.be.equal(true)
  })
  it("should check if a 3 levels property is in an object", () => {
    let obj = {
      first: {
        second: [
          "one",
          {
            third: {
              ok: "cool"
            }
          }
        ]
      }
    }

    expect(has(obj, "first.second.1.third.ok")).to.be.true
  })
})
