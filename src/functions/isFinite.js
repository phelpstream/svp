export default function isFinite(nb) {
  return Number.isFinite(nb)
}
