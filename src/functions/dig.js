import each from "./each"
import { isIterable } from "./is"
import iteratingItem from "./iteratingItem"

/* eslint-disable no-empty */

/**
 * Loop recursively through an iterable data structure.
 * @param {*} obj The value to iterate over.
 * @param {Function} iterateeFn The iterateeFn to look at the data.
 */
export default function dig(obj, iterateeFn = (val, key, path) => val, itemFn = (item, val, key, path) => null) { // eslint-disable-line no-unused-vars

  return each(
    obj,
    function iterator(eachVal, eachKey, eachPath) {
      if (isIterable(eachVal, { objects: true })) {
        return iterateeFn(each(eachVal, iterator, i => null, eachPath), eachKey, eachPath) // eslint-disable-line no-unused-vars
      }
      return iterateeFn(eachVal, eachKey, eachPath)
    },
    function itemIterator(item, eachVal, eachKey, eachPath) {
      let result = itemFn(iteratingItem(eachVal, eachKey, eachPath), eachVal, eachKey, eachPath)
      // let result = itemFn(iteratingItem(eachVal, eachKey, eachPath), eachVal, eachKey, eachPath)

      // console.log("result.newValue()", result.newValue())

      let status = undefined
      try {
        status = result.status()
      } catch (error) {}

      if (status && !status.remove && !status.valueSet && status.next && isIterable(status.next, { objects: true })) {
        let childValue = each(status.next, v => v, itemIterator, eachPath)
        result.set(childValue)
        return result
      } else if (status) {
        return result
      }
    },
    ""
  )
}

// tests

// import isEmpty from "./isEmpty"
// import iftr from "./iftr"

// function modifier(value) {
//   let result = iftr(!isEmpty(value), value)
//   console.log("Value:", value, "Result:", result)
//   return result
// }

// dig({ amazing: [{ ok: ["wahou"], other: "", another: [] }], ok: true }, (val, key, iterableParent, path) => {
//   console.log("path", path)
// }, "")

// console.log(
//   dig(
//     { one: 1, thirtytwo: 32, five: 5, six: 6, nine: 9, ok: { un: 1, deux: 2, trois: { ok: false, no: true } } },
//     (val, key, iterableParent, path, next) => {
//       // console.log("path:", path);
//       // console.log("key", key);
//       if (key === "un") return "ONE"
//       // if (isObject(val) && has(val, "un")) return next(val)
//     }
//   )
// )

// console.log(
//   dig({ one: 1, thirtytwo: 32, five: 5, six: 6, nine: 9, ok: { un: 1, deux: 2, trois: { ok: 34, no: 33 } } }, (item, val, key, path) => {
//     if (isInteger(item.value()) && item.value() % 2 === 0) {
//       item.remove()
//     }
//     return item
//     // console.log("path", path)
//     // return "ok"
//   })
// )

// console.dir(
//   dig(
//     {
//       article: {
//         detail: {
//           ok: {
//             title: "h2",
//             subtitle: "h3",
//             posts: {
//               $s: "#posts ul li",
//               $d: [
//                 {
//                   title: ".title",
//                   text: ".text"
//                 }
//               ]
//             }
//           }
//         }
//       }
//     },
//     (item, val, key, path) => {
//       if (isObject(item.value()) && has(item.value(), "$s")) {
//         let selector = get(item.value(), "$s")
//         let data = get(item.value(), "$d")
//         // fake a fetch of data
//         item.set(selector + "_" + data)
//       } else if (isObject(item.value())) {
//         item.next()
//       } else {
//         item.remove()
//       }
//       return item
//     }
//   ), {
//     depth: 4
//   }
// )
// let value = {
//   one: {
//     a: "a",
//     b: "b",
//     c: "c"
//   },
//   two: {
//     a: "a",
//     b: "b"
//   },
//   three: "t"
// }
// let iteratee = (val, key, path) => (typeof val === "string" ? val.toUpperCase() : val) // eslint-disable-line no-unused-vars
// let uppercasedObj = dig(value, iteratee)
// console.log(JSON.stringify(uppercasedObj, null, 2))
