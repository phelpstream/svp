import join from "./join"
import reverse from "./reverse"

export default function joinFlip(list, ...args) {
  return join(reverse(list), ...args)
}
