import isOdd from "./isOdd"
import not from "./not"

export default not(isOdd)