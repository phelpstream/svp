import get from "./get"

export default function typeFn(value) {
  let types = {
    Object: Object,
    Array: Array,
    String: String,
    Number: Number
  }
  let valueConstructorName = get(value, "constructor.name", null)
  return valueConstructorName && get(types, valueConstructorName, {})
}
