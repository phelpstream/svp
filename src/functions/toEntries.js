import { isObject, isString, isNumber, isIterable, isArray, isEmpty } from "./is"
import toArray from "./toArray"
import clone from "./clone"

/**
 *
 * @param {*} value The value to convert to entries (key, val)
 */
export default function toEntries(value, mutable = true) {
  let entries = []
  if (isArray(value)) entries = Object.entries(value)
  if (isEmpty(entries) && isObject(value)) entries = toArray(Object.entries(value))
  if (isEmpty(entries) && (isString(value) || isNumber(value))) entries = toArray(toArray(`${value}`).entries())
  if (isEmpty(entries) && isIterable(value)) entries = toArray(value.entries())
  return mutable ? entries : entries.map(([index, value]) => [index, clone(value)])
}

// tests

// let value = [1, 2, 3]
// let entries = toEntries(value)
// console.log(entries)
