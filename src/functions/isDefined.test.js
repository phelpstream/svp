import { expect } from "chai"
import VARS from "./../consts/tests.vars"
import isDefined from "./isDefined"

const testName = "DEFINED"

describe("isDefined.js", () => {
  // NUMBERS
  it("should find that a(n) [NUMBER.INFINITY] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.NUMBER.INFINITY)
    expect(result).to.be.true
  })
  it("should find that a(n) [NUMBER.INFINITY_CALC] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.NUMBER.INFINITY_CALC)
    expect(result).to.be.true
  })
  it("should find that a(n) [NUMBER.MINUS_INFINITY_CALC] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.NUMBER.MINUS_INFINITY_CALC)
    expect(result).to.be.true
  })
  it("should find that a(n) [NUMBER.INTEGER] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.NUMBER.INTEGER)
    expect(result).to.be.true
  })
  it("should find that a(n) [NUMBER.FLOAT] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.NUMBER.FLOAT)
    expect(result).to.be.true
  })
  it("should find that a(n) [NUMBER.LONG_FLOAT] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.NUMBER.LONG_FLOAT)
    expect(result).to.be.true
  })

  // STRINGS
  it("should find that a(n) [STRING.EMPTY] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.STRING.EMPTY)
    expect(result).to.be.true
  })
  it("should find that a(n) [STRING.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.STRING.SIMPLE)
    expect(result).to.be.true
  })

  // OBJECTS
  it("should find that a(n) [OBJECT.EMPTY] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.OBJECT.EMPTY)
    expect(result).to.be.true
  })
  it("should find that a(n) [OBJECT.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.OBJECT.SIMPLE)
    expect(result).to.be.true
  })

  // BOOLEANS
  it("should find that a(n) [BOOLEAN.TRUE] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.BOOLEAN.TRUE)
    expect(result).to.be.true
  })
  it("should find that a(n) [BOOLEAN.FALSE] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.BOOLEAN.FALSE)
    expect(result).to.be.true
  })

  // ARRAYS
  it("should find that a(n) [ARRAY.EMPTY] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.ARRAY.EMPTY)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.STRING] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.ARRAY.STRING)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.INTEGER] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.ARRAY.INTEGER)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.UNDEFINED] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.ARRAY.UNDEFINED)
    expect(result).to.be.true
  })

  // SETS
  it("should find that a(n) [SET.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.SET.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [SET.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.SET.SIMPLE)
    expect(result).to.be.true
  })

  // WEAKSETS
  it("should find that a(n) [WEAKSET.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.WEAKSET.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [WEAKSET.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.WEAKSET.SIMPLE)
    expect(result).to.be.true
  })

  // MAPS
  it("should find that a(n) [MAP.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.MAP.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [MAP.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.MAP.SIMPLE)
    expect(result).to.be.true
  })

  // WEAKMAPS
  it("should find that a(n) [WEAKMAP.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.WEAKMAP.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [WEAKMAP.SIMPLE] value [IS] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.WEAKMAP.SIMPLE)
    expect(result).to.be.true
  })

  // SPECIAL CASES
  it("should find that a(n) [NOT INSTANCIATED] value [IS NOT] (an) [" + testName + "]", () => {
    let value
    let result = isDefined(value)
    expect(result).to.be.false
  })
  it("should find that a(n) [UNDEFINED] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.UNEFINED)
    expect(result).to.be.false
  })
  it("should find that a(n) [NULL] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isDefined(VARS.NULL)
    expect(result).to.be.true
  })
})
