import { expect } from "chai"

import baseGet from "./baseGet"

describe("baseGet.js", () => {
  it("should return the key value of an object", () => {
    let obj = { ok: "good", no: "bad" }
    expect(baseGet(obj, "ok")).to.be.equal("good")
  })
})
