import { expect } from "chai"

import sortObj from "./sortObj"

describe("sortObj.js", () => {
  it("should sort an object by key values", () => {
    let obj = { b: true, a: false }
    expect(sortObj(obj)).to.be.eql({a: false, b: true})
  })
})
