export default function unshift(arr, ...values) {
  return arr.unshift(...values) && arr
}
