import curry from "./curry"
import isType from "./isType"

let isBoolean = curry(isType)("boolean")
export default isBoolean
