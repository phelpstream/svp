import { isObject, isString, isNumber, isArray } from "./is" // isIterable
import toArray from "./toArray"
import clone from "./clone"

/**
 *
 * @param {*} value The value to convert to entries (key, val)
 */
export default function toValues(value, mutable = true) {
  let values = []
  if (isArray(value)) values = Object.values(value)
  if (isObject(value)) values = Object.values(value)
  if (isString(value) || isNumber(value)) values = toArray(toArray(`${value}`).values())
  // if (isIterable(value)) values = toArray(value.values())
  return mutable ? values : isArray(values) ? values.map(v => clone(v)) : values
}

// let o = {
//   o: {
//     raw: "1938038"
//   }
// }

// console.log(toValues(o))
