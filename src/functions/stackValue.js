import entries from "./toEntries"
import keys from "./toKeys"
import get from "./get"
import has from "./has"
import length from "./length"
import isArray from "./isArray"
import isDefined from "./isDefined"

export default function stackValue(initialValue) {
  if (!(this instanceof stackValue)) return new stackValue(initialValue)
  let valueStack = {}
  let lastIndex = null

  this.sortedValues = function() {
    return entries(valueStack)
      .sort(([keyA], [keyB]) => keyA - keyB)
      .map(keyValue => keyValue[1])
  }

  this.push = function(withIndex = -1, value, meta) {
    let lastValue, newValue
    if (has(valueStack, `${withIndex}`)) {
      lastValue = this.get(withIndex)
      newValue = !isArray(lastValue) ? [lastValue] : lastValue
      newValue.push(value)
    } else {
      newValue = [value]
    }
    return this.set(newValue, meta)
  }

  this.value = function(value, meta) {
    if (isDefined(value)) return this.set(value, meta)
    return this.get()
  }

  this.set = function(value, meta = {}) {
    let valueStackLength = length(keys(valueStack))
    valueStack[`${valueStackLength}`] = { data: value, _t: new Date().getTime(), meta, index: valueStackLength }
    lastIndex = valueStackLength
    return { [`${valueStackLength}`]: valueStack[`${valueStackLength}`] }
  }

  this.get = this.toString = this.toValue = function(index = 0) {
    return get(this.sortedValues(), `${index}.data`, null)
  }

  this.getFull = this.toString = this.toValue = function(index = 0) {
    return get(this.sortedValues(), `${index}`, null)
  }

  this.lastIndex = function() {
    return lastIndex
  }

  this.last = function() {
    return get(this.sortedValues().reverse(), "0.data", null)
  }

  this.first = function() {
    return get(this.sortedValues(), "0.data", null)
  }

  this.history = function() {
    return valueStack
  }

  if (initialValue) {
    this.set(initialValue)
  }

  return this
}

// let v = stackValue()
// console.dir(v.set({ ok: true }))
// let v = "ok"
// console.log(isDefined(v));
// console.log(v);

// console.log(v.value({ ok: false }));
// console.dir(v)
// console.dir(v.history(), { depth: 3 })
// console.log(v.last())
// console.log(v.value())
