import { expect } from "chai"

import toPath from "./toPath"

describe("toPath.js", () => {
  it("should parse a string composed of keys to a path (array)", () => {
    expect(toPath("a.b.c.d")).to.eql(["a", "b", "c", "d"])
  })
  it("should parse a string composed of keys and indexes to a path (array)", () => {
    expect(toPath("a.b[3].c[2].d")).to.eql(["a", "b", "3", "c", "2", "d"])
  })
})
