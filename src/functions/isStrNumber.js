export default function isStrNumber(str) {
  return /^\d+$/.test(str)
}

// console.log(isStrNumber("1"))
