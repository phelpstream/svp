import { isString, isNumber, isIterable, isArray } from "./is"
import toArray from "./toArray"
import arrToObj from "./arrToObj"

export default function toObject(value) {
  if (isArray(value)) return arrToObj(value)
  else if (isIterable(value)) return toObject(toArray(value))
  else if (isString(value) || isNumber(value)) return toObject(toArray(`${value}`))
  return value
}
