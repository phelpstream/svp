import isNumber from "./isNumber"

export default function isOdd(nb) {
  return isNumber(nb) && nb % 2 !== 0
}
