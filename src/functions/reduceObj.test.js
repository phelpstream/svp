import { expect } from "chai"

import reduceObj from "./reduceObj"

describe("reduceObj.js", () => {
  it("should reduce to inverse values", () => {
    let obj = { b: true, a: false }
    expect(
      reduceObj(obj, function(finalObj, value, key, index) { // eslint-disable-line no-unused-vars
        finalObj[key] = !value
      })
    ).to.be.eql({ b: false, a: true })
  })
})
