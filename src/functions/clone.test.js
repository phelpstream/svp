import { expect } from "chai"

import clone from "./clone"

describe("clone.js", () => {
  it("should clone an object (no possible mutations)", () => {
    let obj1 = { a: 1 }
    let obj2 = clone(obj1)
    obj1.a = 2
    expect(obj2).to.contain({ a: 1 })
  })
  it("should clone an array (no possible mutations)", () => {
    let arr1 = [1, 2, 3]
    let arr2 = clone(arr1)
    arr1.push(4)
    expect(arr2).to.not.contain(4)
  })
  it("should clone a string (no possible mutations)", () => {
    let str1 = "a string"
    let str2 = clone(str1)
    str1 = "another string"
    expect(str2).to.be.equal("a string")
  })
})
