import assign from "./assign"
import create from "./create"

export default function extend(targetObj, ...sourceObjects) {
  return assign(create(targetObj), ...sourceObjects)
}
