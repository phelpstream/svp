export default function replaceAt(str, index, char) {
  let arr = Array.from(str)
  arr[index] = char
  return arr.join("")
}
