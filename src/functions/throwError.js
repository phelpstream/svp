import throwIt from "./throwIt"

export default function trowError(error) {
  return throwIt(new Error(error))
}
