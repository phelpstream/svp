import isEmpty from "./isEmpty"
import not from "./not"

export default not(isEmpty)
