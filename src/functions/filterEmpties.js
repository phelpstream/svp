import filter from "./filter"
import isNotEmpty from "./isNotEmpty"

export default function filterEmpties(value) {
  return filter(v => isNotEmpty(v), value)
}

// console.log(
//   filterEmpties({
//     faxes: {
//       "0": {
//         raw: ""
//       }
//     },
//     phones: {
//       "0": {
//         raw: ""
//       }
//     },
//     emails: {
//       "0": {
//         address: ""
//       }
//     }
//   })
// )
