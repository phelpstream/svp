import curry from "./curry"
import isType from "./isType"

let isSet = curry(isType)("set")
export default isSet