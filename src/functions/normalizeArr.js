import sortArr from "./sortArr"

export default function normalizeArr(arr, sortFn) {
  return sortArr(arr, sortFn)
}
