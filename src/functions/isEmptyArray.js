export default function isEmptyArray(arr) {
  return arr && arr.length === 0
}
