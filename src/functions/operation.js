export default function operation(fn = (total, n) => total + n, nb, ...nbs) {
  return nbs.reduce(fn, nb)
}

// console.log(operation((t, v) => t - 1, 1, 2, 3, 4))
