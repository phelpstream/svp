import entries from "./toEntries"
import values from "./toValues"
import includes from "./includes"
// tests
// import has from "./has"
// import remove from "./remove"

export default function unique(obj, valuePreProcessorFn = v => v) {
  return entries(obj).reduce((uniqueValues, [key, value]) => {
    if (!includes(values(uniqueValues), valuePreProcessorFn(value))) {
      uniqueValues[key] = value
    }
    return uniqueValues
  }, {})
}

// tests

// let obj = {
//   a: "Super",
//   b: "Super",
//   c: {
//     uid: "1",
//     ok: true
//   },
//   d: {
//     uid: "2",
//     ok: true
//   }
// }

// console.dir(
//   unique(obj, value => {
//     if (has(value, "uid")) return remove(value, "uid")
//     return value
//   })
// )
