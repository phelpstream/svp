import { expect } from "chai"

"use strict"
import assign from "./assign"

describe("assign.js", () => {
  it("should create a new object with combined properties", () => {
    let obj1 = { a: 1 },
      obj2 = { b: 2 }
    expect(assign(obj1, obj2)).to.be.eql({ a: 1, b: 2 })
  })

  it("should create a new object with merged properties", () => {
    let obj1 = { a: 1 },
      obj2 = { a: 2, b: 2 }
    expect(assign(obj1, obj2)).to.be.eql({ a: 2, b: 2 })
  })
})
