import { expect } from "chai"

import concat from "./concat"

describe("concat.js", () => {
  it("should concatenate several arrays in one (no mutations or linked data)", () => {
    let arr1 = [1]
    let arr2 = [2]
    let concatenatedArr = concat(arr1, arr2)
    arr1.push(3)
    arr2.push(4)
    expect(concatenatedArr).to.be.eql([1, 2])
  })
})
