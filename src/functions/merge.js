//@ts-check
import isObject from "./isObject"
import isArray from "./isArray"

/**
 * Recursively merge two objects.
 * @param target
 * @param ...sources
 */
export default function merge(target, ...sources) {
  if (!sources.length) return target
  const source = sources.shift()

  if (isObject(target) && isObject(source)) {
    for (const key in source) {
      let sourceIsArray = isArray(source[key])
      let targetIsArray = isArray(target[key])
      if ((sourceIsArray || targetIsArray) && source[key] && target[key]) {
        if (targetIsArray && sourceIsArray) target[key].push(...source[key])
        else if (targetIsArray && !sourceIsArray) target[key].push(source[key])
        else if (!targetIsArray && sourceIsArray) target[key] = [target[key], ...source[key]]
      } else if (isObject(source[key])) {
        if (!target[key]) Object.assign(target, { [key]: {} })
        merge(target[key], source[key])
      } else {
        Object.assign(target, { [key]: source[key] })
      }
    }
  }

  return merge(target, ...sources)
}

// let a = {
//   un: [1, 2]
// }

// let b = {
//   un: [2, 4],
//   ok: [2]
// }

// console.log(merge(a, b))
