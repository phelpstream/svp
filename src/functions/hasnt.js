import has from "./has"
import not from "./not"

export default not(has)
