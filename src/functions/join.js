// export default function join([str, ...strs], joiner = "") {
//   return strs.reduce((text, s) => `${text}${joiner}${s}`, str)
// }
export default function join(strs, joiner = "") {
  return strs.join(joiner)
}
