import clone from "./clone"
import splice from "./splice"

export default function spliceSoft(arr, startIndex, itemsNbToDelete, ...valuesToInject) {
  let _arr = clone(arr)
  return splice(_arr, startIndex, itemsNbToDelete, ...valuesToInject) && _arr
}
