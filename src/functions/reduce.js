import { isObject, isArray } from "./is"
import reduceArr from "./reduceArr"
import reduceObj from "./reduceObj"

export default function reduce(value, reducerFn, startValue, ...args) {
  if (isObject(value)) return reduceObj(value, reducerFn, startValue, ...args)
  if (isArray(value)) return reduceArr(value, reducerFn, startValue, ...args)
}
