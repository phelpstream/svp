import { isString, isNumber } from "./is"
export default function surround(
  text,
  startIndex,
  stringOrLength,
  { arround, left, right, innerLeftValue = "", innerRightValue = "", outerLeftValue = "", outerRightValue = "", maxLength } = {}
) {
  let inputSpaceLength = isString(stringOrLength) ? stringOrLength.length : isNumber(stringOrLength) ? stringOrLength : null
  let middleResult = ""
  let leftResult = ""
  let rightResult = ""
  let leftSpace = ""
  let rightSpace = ""
  let leftBound = startIndex
  let rightBound = startIndex + inputSpaceLength
  if (text.length >= startIndex && inputSpaceLength) {
    if (arround) {
      middleResult = text.slice(leftBound, rightBound)
      leftBound = startIndex - arround
      if (leftBound < 0) leftBound = 0
      rightBound = startIndex + inputSpaceLength + arround
      if (rightBound > text.length) rightBound = text.length
    } else if (left || right) {
      middleResult = text.slice(leftBound, rightBound)
      if (left) {
        leftBound = leftBound - left
        if (leftBound < 0) leftBound = 0
      }
      if (right) {
        rightBound = rightBound + right
        if (rightBound > text.length) rightBound = text.length
      }
    }

    leftResult = text.slice(leftBound, startIndex)
    rightResult = text.slice(startIndex + inputSpaceLength, rightBound)

    if (maxLength) {
      let valuesLength = outerLeftValue.length + outerRightValue.length + innerLeftValue.length + innerRightValue.length
      let spaceValue = maxLength - (valuesLength + inputSpaceLength) / 2
      if (spaceValue > 0) {
        leftSpace = " ".repeat(Math.floor(spaceValue))
        rightSpace = " ".repeat(Math.ceil(spaceValue))
      }
    }
  }
  return `${outerLeftValue}${leftSpace}${leftResult}${innerLeftValue}${middleResult}${innerRightValue}${rightResult}${rightSpace}${outerRightValue}`
}

// let t = "A super long text with some nice words and quite some context, or not."
// let str = "some"
// let index = 23

// console.log(surround(t, index, str, { left: 5, right: 10, innerLeftValue: " [[ ", innerRightValue: " ]] ", outerLeftValue: "(", outerRightValue: ")" }))
