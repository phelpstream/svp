export default function baseSort(value, sortFn = a => a, keyToSortOn) {
  return value.sort((a, b) => {
    let aVal = typeof keyToSortOn !== "undefined" ? a[keyToSortOn] : a
    let bVal = typeof keyToSortOn !== "undefined" ? b[keyToSortOn] : b
    return sortFn(aVal, bVal)
  })
}
