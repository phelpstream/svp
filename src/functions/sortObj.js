import reduceObj from "./reduceObj"
import sortEntries from "./sortEntries"

export default function sortObj(obj, sortFn) {
  return reduceObj(obj, (finalObj, value, key) => (finalObj[key] = value), {}, sortEntries, sortFn)
}
