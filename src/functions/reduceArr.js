export default function reduceArr(arr, reducerFn, startValue) {
  return arr.reduce(reducerFn, startValue)
}
