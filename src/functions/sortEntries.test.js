import { expect } from "chai"

import sortEntries from "./sortEntries"

describe("sortEntries.js", () => {
  it("should sort the entries by key", () => {
    // let obj = { b: true, a: false }
    let entries = [["b", true], ["a", false]]
    expect(sortEntries(entries)).to.be.eql([["a", false], ["b", true]])
  })
})
