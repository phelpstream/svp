"use strict"
import cloneObj from "./cloneObj"
import cloneArr from "./cloneArr"
import cloneStr from "./cloneStr"

import isObject from "./isObject"
import isArray from "./isArray"
import isString from "./isString"

export default function clone(value) {
  if (isArray(value)) return cloneArr(value)
  if (isObject(value)) return cloneObj(value)
  if (isString(value)) return cloneStr(value)
  return value
}
