import dig from "./dig"
import sortObj from "./sortObj"
import sortArr from "./sortArr"
import { isObject, isArray } from "./is"

export default function normalizeObj(obj, sortFn) {
  let _obj = sortObj(obj, sortFn)
  return dig(_obj, function(val, key) { // eslint-disable-line no-unused-vars
    if (isObject(val)) return sortObj(val)
    else if (isArray(val)) return sortArr(val)
    else return val
  })
}
