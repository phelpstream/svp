import get from "./get"
import { isArray, isString } from "./is"
import dig from "./dig"
import reduce from "./reduce"
import toEntries from "./toEntries"
import surround from "./surround"

export default function capture(value, regex, { multiple = true, merge = false, keys = null, hightlightString, highlightSpaceArround = 20 } = {}) {
  let results = []
  let flags = regex.flags || ""
  let localRegex = new RegExp(regex.source, regex.flags)

  if (!flags.includes("g")) localRegex = new RegExp(regex.source, `${flags}g`)
  if (isString(value)) {
    let resultTable
    let continueToLoop = true
    while (continueToLoop && (resultTable = localRegex.exec(value)) !== null) {
      if (resultTable) {
        if (isArray(keys)) {
          let captureResult = {}
          for (let [keyIndex, key] of toEntries(keys)) {
            captureResult[key] = get(resultTable.slice(1), keyIndex, null)
          }
          results.push(captureResult)
        } else {
          let captureResult = {
            input: get(resultTable, "input"),
            matched: get(resultTable, "0"),
            index: get(resultTable, "index"),
            groups: resultTable.slice(1)
          }
          try {
            captureResult.highlight = surround(captureResult.input, captureResult.index, captureResult.matched, {
              arround: highlightSpaceArround,
              innerLeftValue: hightlightString || " >> ",
              innerRightValue: hightlightString || " << "
            })
          } catch (error) {
            // console.error(error)
          }
          results.push(captureResult)
        }
      }
      if (!multiple) continueToLoop = false
    }
    if (multiple && merge) {
      results = reduce(
        results,
        (groups, result) => {
          let resultGroups = get(result, "groups", [])
          groups.push(...resultGroups)
          return groups
        },
        []
      )
    }
  } else {
    let resultTable
    dig(value, (itValue, itKey, itObj, path) => {
      resultTable = localRegex.exec(itValue)
      let captureResult = {
        input: get(resultTable, "input"),
        matched: get(resultTable, "0"),
        index: get(resultTable, "index"),
        path: path,
        groups: resultTable.slice(1)
      }
      try {
        captureResult.highlight = surround(captureResult.input, captureResult.index, captureResult.matched, {
          arround: 20,
          innerLeftValue: hightlightString || " _|| ",
          innerRightValue: hightlightString || " ||_ "
        })
      } catch (error) {
        // console.error(error)
      }
      results.push(captureResult)
    })
  }

  return multiple ? results : get(results, "0", null)
}

// exemple

// let s = "Some boring text"
// let r = /(\w+)/gim

// let result = capture(s, r, { multiple: true, merge: true })
// console.dir(result)

// let r = /(?:\$)(\w+)(?:\:)(\d+)*/g
// let r = /(?:\$)(\w+)(?::)(\d+)*/
// let s = "$text:2:contains('Test')"
// let s = "$dad:1 $text:3:contains('Test')"

// let s1 = "$parent(' super '):,4"
// let s2 = "$text('something esl'):contains('Test')"
// let r = /(?:\$)(\w+)(?:(?:\()(.+?)(?:\)))*(?:(?:\:)([\d,\+]+))*(.+)*/g
// console.log("r", r)
// console.dir(capture(s1, r, { keys: ["selector", "params", "getter", "pseudoclass"], multiple: false }))
// console.log("r", r)
// console.dir(capture(s2, r, { keys: ["selector", "params", "getter", "pseudoclass"], multiple: false }))
// console.dir(capture(s, r, { multiple: true }), { colors: true })
// console.dir(capture(s, r, { multiple: false, keys: ["function", "value"] }), { colors: true })
// console.dir(capture(s, r, { keys: ["function", "value"] }), { colors: true })
