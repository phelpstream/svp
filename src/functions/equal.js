import normalize from "./normalize"
import stringify from "./stringify"

export default function equal(firstValue, ...values) {
  return values.every(value => stringify(normalize(value)) === stringify(normalize(firstValue)))
}
