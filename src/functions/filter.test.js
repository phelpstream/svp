import { expect } from "chai"

import filter from "./filter"

describe("filter.js", () => {
  it("filter null value from object", () => {
    expect(filter(v => v !== null, { yes: true, no: null })).to.be.eql({ yes: true })
  })
})
