import baseToString from "./baseToString"

export default function toString(value) {
	return value == null ? '' : baseToString(value);
}
