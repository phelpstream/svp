import cloneArr from "./cloneArr"

export default function reverse(arr) {
  let _arr = cloneArr(arr)
  return _arr.reverse()
}
