import { expect } from "chai"

import invoke from "./invoke"

describe("invoke.js", () => {
  it("builds an function ahead and can be invoked on an object (here)", () => {
    let obj = { one: true, two: "ok" }
    let toString = invoke("toString")
    expect(obj.toString()).to.be.eql(toString(obj))
  })
})
