import some from "./some"
import equal from "./equal"

export default function includes(valueToBeSearched, valuetoSearchFor) {
  return some(valueToBeSearched, value => equal(value, valuetoSearchFor))
}
