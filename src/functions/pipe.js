/**
 * 
 * @param {*} fns 
 */
export default function pipe(...fns) {
  return (...args) => fns.reduce((result, fn) => (Array.isArray(result) ? fn(...result) : fn(result)), args)
}
