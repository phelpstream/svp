"use strict"
export default function unary(fn) {
  return function(arg1) {
    return fn(arg1)
  }
}
