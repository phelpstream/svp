import { expect } from "chai"

import create from "./create"

describe("create.js", () => {
  it("should create a new object linked to the object prototypes/properties", () => {
    let obj = {
      _name: "Gabin",
      name: function() {
        return this._name
      }
    }
    expect(create(obj)).to.be.eql(obj)
  })
})
