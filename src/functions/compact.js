export default function compact(str) {
  return str.replace(/\s+/gm, " ")
}
