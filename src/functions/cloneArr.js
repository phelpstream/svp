"use strict"
export default function cloneArr(arr) {
  return [].slice.call(arr)
}
