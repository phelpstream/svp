import { expect } from "chai"

import equal from "./equal"

describe("equal.js", () => {
  it("should return if equal strings", () => {
    expect(equal("One", "one")).to.be.true
  })
  it("should return if equal arrays", () => {
    expect(equal(["one"], ["one"])).to.be.true
  })
  it("should return if equal objects", () => {
    expect(equal({ one: 1, two: 2 }, { two: 2, one: 1 })).to.be.true
  })
})
