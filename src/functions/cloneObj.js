"use strict"
import extend from "./extend"

export default function cloneObj(obj) {
  return extend({}, obj)
}
