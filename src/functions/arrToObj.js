export default function arrToObj(value) {
  return value.reduce((obj, val, index) => {
    obj[index] = val
    return obj
  }, {})
}
