"use strict"
import { isString, isArray, isNumber, isStrNumber, isObject } from "./is"
// import each from "./each"
// import join from "./join"
import stringify from "./stringify"
import toArray from "./toArray"

export default function baseToString(value) {
  let valueToString = value.toString || null
  if (isString(value)) return value
  if (isArray(value)) return value.map(baseToString).join(" ")
  if (isNumber(value)) return `${value}`
  if (isObject(value)) {
    if (Object.entries(value).every(([key]) => isStrNumber(key))) {
      return toArray(value).join("")
    }
    return stringify(value)
  }
  if (valueToString) return value.toString()
  return value
}

// let o = { "4": "D", "1": "G" }
// let o = { hello: "you" }
// console.log(baseToString(o))
