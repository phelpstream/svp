import some from "./some"

export default function or(...args) {
  return some(args, testResult => testResult === true)
}
