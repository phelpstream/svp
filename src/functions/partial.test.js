import { expect } from "chai"

import partial from "./partial"

describe("partial.js", () => {
  it("create a new function loading the first parameter first", () => {
    let add = (a, b) => a + b
    let add2 = partial(add, 2)
    expect(add2(5)).to.be.equal(7)
  })
})
