import { toObject, toArray, toString } from "./to"
import { isString, isArray } from "./is"
import clone from "./clone"
import remove from "./remove"
import iteratingItem from "./iteratingItem"

/* eslint-disable no-empty */

/**
 * Loop through the first level of an iterable data structure.
 * @param {*} value The value to iterate over.

 * @param {Function} iterateeFn The iterateeFn to look at the data.
 */

export default function each(value, iterateeFn = (val, key, path) => val, itemFn = (item, val, key, newPath) => null, startPath = "") { // eslint-disable-line no-unused-vars
  // modifierFn = v => v,
  let _value = clone(toObject(value))

  let convertBackFn = v => v
  if (isArray(value)) convertBackFn = toArray
  else if (isString(value)) convertBackFn = toString

  for (let [key, val] of Object.entries(_value)) {
    let newPath = startPath.length > 0 ? `${startPath}.${key}` : key
    let item = iteratingItem(val, key, newPath)
    let result = itemFn(item, val, key, newPath) || iterateeFn(val, key, newPath)

    let status = undefined
    try {
      status = result.status()
    } catch (error) {}

    if (status) {
      if (status.remove) {
        remove(_value, key, true)
      } else {
        _value[key] = status.value
        // _value[key] = !isNull(status.value) ? status.value : val
      }
    } else if (result) {
      _value[key] = result || val
    } else {
      _value[key] = val
    }
  }
  return convertBackFn(_value)
}

// tests

// import iftr from "./iftr"
// import isEmpty from "./isEmpty"

// console.log(each({ amazing: [{ ok: [] }], ok: true }, value => iftr(!isEmpty(value), value)))
// console.log(each([1, 32, 5, 6, 9], value => iftr(value > 5, value)))
// console.log(each({ one: 1, thirtytwo: 32, five: 5, six: 6, nine: 9 }, value => iftr(value > 5, value)))
// console.log(
//   each({ one: 1, thirtytwo: 32, five: 5, six: 6, nine: 9 }, (item, val, key, path) => {
//     if (item.value() % 2 === 0) {
//       item.remove()
//     }
//     return item
//     // console.log("path", path)
//     // return "ok"
//   })
// )
