import curry from "./curry"
import isType from "./isType"

export default curry(isType)("symbol")
