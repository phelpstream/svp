import isDefined from "./isDefined"

export default function some(value, someFn = (value, index, array) => isDefined(value)) { // eslint-disable-line no-unused-vars
  return value.some(someFn)
}
