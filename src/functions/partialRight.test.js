import { expect } from "chai"

import partialRight from "./partialRight"

describe("partialRight.js", () => {
  it("create a new function loading the last parameter first", () => {
    let minus = (a, b) => a - b
    let minus2 = partialRight(minus, 2)
    expect(minus2(5)).to.be.equal(3)
  })
})
