import curry from "./curry"
import isType from "./isType"

let isMap = curry(isType)("map")
export default isMap