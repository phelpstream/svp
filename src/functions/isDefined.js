"use strict"
import isNotDefined from "./isNotDefined"
import not from "./not"

export default not(isNotDefined)
