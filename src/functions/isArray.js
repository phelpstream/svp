"use strict"
import curry from "./curry"
import isType from "./isType"

export default function isArray(arr, lengthEqualOrhigherThan = 0) {
  let isValid = curry(isType)("array")
  return isValid(arr) && arr.length >= lengthEqualOrhigherThan
}

// let a = [1, 2, 3, 4]
// console.log(isArray(a, 5))
