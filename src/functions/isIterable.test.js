import { expect } from "chai"
import VARS from "./../consts/tests.vars"
import isIterable from "./isIterable"

const testName = "ITERABLE"

describe("isIterable.js", () => {
  // NUMBERS
  it("should find that a(n) [NUMBER.INFINITY] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.NUMBER.INFINITY)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.INFINITY_CALC] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.NUMBER.INFINITY_CALC)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.MINUS_INFINITY_CALC] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.NUMBER.MINUS_INFINITY_CALC)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.INTEGER] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.NUMBER.INTEGER)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.FLOAT] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.NUMBER.FLOAT)
    expect(result).to.be.false
  })
  it("should find that a(n) [NUMBER.LONG_FLOAT] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.NUMBER.LONG_FLOAT)
    expect(result).to.be.false
  })

  // STRINGS
  it("should find that a(n) [STRING.EMPTY] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.STRING.EMPTY)
    expect(result).to.be.false
  })
  it("should find that a(n) [STRING.SIMPLE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.STRING.SIMPLE)
    expect(result).to.be.false
  })

  // OBJECTS
  it("should find that a(n) [OBJECT.EMPTY] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.OBJECT.EMPTY)
    expect(result).to.be.false
  })
  it("should find that a(n) [OBJECT.SIMPLE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.OBJECT.SIMPLE)
    expect(result).to.be.false
  })

  // BOOLEANS
  it("should find that a(n) [BOOLEAN.TRUE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.BOOLEAN.TRUE)
    expect(result).to.be.false
  })
  it("should find that a(n) [BOOLEAN.FALSE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.BOOLEAN.FALSE)
    expect(result).to.be.false
  })

  // ARRAYS
  it("should find that a(n) [ARRAY.EMPTY] value [IS] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.ARRAY.EMPTY)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.STRING] value [IS] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.ARRAY.STRING)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.INTEGER] value [IS] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.ARRAY.INTEGER)
    expect(result).to.be.true
  })
  it("should find that a(n) [ARRAY.UNDEFINED] value [IS] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.ARRAY.UNDEFINED)
    expect(result).to.be.true
  })

  // SETS
  it("should find that a(n) [SET.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.SET.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [SET.SIMPLE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.SET.SIMPLE)
    expect(result).to.be.false
  })

  // WEAKSETS
  it("should find that a(n) [WEAKSET.INSTANCE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.WEAKSET.INSTANCE)
    expect(result).to.be.false
  })
  it("should find that a(n) [WEAKSET.SIMPLE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.WEAKSET.SIMPLE)
    expect(result).to.be.false
  })

  // MAPS
  it("should find that a(n) [MAP.INSTANCE] value [IS] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.MAP.INSTANCE)
    expect(result).to.be.true
  })
  it("should find that a(n) [MAP.SIMPLE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.MAP.SIMPLE)
    expect(result).to.be.false
  })

  // WEAKMAPS
  it("should find that a(n) [WEAKMAP.INSTANCE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.WEAKMAP.INSTANCE)
    expect(result).to.be.false
  })
  it("should find that a(n) [WEAKMAP.SIMPLE] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.WEAKMAP.SIMPLE)
    expect(result).to.be.false
  })

  // SPECIAL CASES
  it("should find that a(n) [NOT INSTANCIATED] value [IS NOT] (an) [" + testName + "]", () => {
    let value
    let result = isIterable(value)
    expect(result).to.be.false
  })
  it("should find that a(n) [UNDEFINED] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.UNEFINED)
    expect(result).to.be.false
  })
  it("should find that a(n) [NULL] value [IS NOT] (an) [" + testName + "]", () => {
    let result = isIterable(VARS.NULL)
    expect(result).to.be.false
  })
})
