import { isArray, isObject, isString } from "./is"
import normalizeArr from "./normalizeArr"
import normalizeObj from "./normalizeObj"
import normalizeStr from "./normalizeStr"

export default function normalize(value, sortFn) {
  if (isArray(value)) return normalizeArr(value, sortFn)
  if (isObject(value)) return normalizeObj(value, sortFn)
  if (isString(value)) return normalizeStr(value)
  return value
}
