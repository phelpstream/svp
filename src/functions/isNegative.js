export default function isNegative(nb) {
  return Object.is(nb, -0)
}
