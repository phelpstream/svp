"use strict"

export default function typeOf(value, strict = false) {
  let constructorName
  if (value && value.constructor) constructorName = value.constructor.name || null
  if (!strict && typeof constructorName === "string") return constructorName.toLowerCase()
  return typeof value
}

// console.log("typeof", typeOf(new Date()));
