import isType from "./isType"
import isArray from "./isArray"

export default function isObject(obj, lengthEqualOrhigherThan = 0) {
  return !!(obj && isType("object", obj) && !isArray(obj)) && Object.keys(obj).length >= lengthEqualOrhigherThan
}

// Added !! because of an issue with EMPTY.STRING
// console.log(isObject(""));

// let o = {
//   one: true,
//   two: true
// }
// console.log(isObject(o, 2))
