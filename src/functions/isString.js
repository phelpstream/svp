import curry from "./curry"
import isType from "./isType"

export default function isString(str, lengthEqualOrhigherThan = 0) {
  let isValid = curry(isType)("string")
  return isValid(str) && str.length >= lengthEqualOrhigherThan
}
