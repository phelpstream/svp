import { isArray, isString } from "./is"
import isNotDefined from "./isNotDefined"
import get from "./get"
import dig from "./dig"

export default function morph(mapObj, inputObj, defaultValue) {
  /* eslint-disable no-unused-vars */
  return dig(mapObj, v => v, function modifier(item, itValue, itKey, itObj) {
    let convertToNumber = false
    if (isString(itValue)) {
      if (itValue.startsWith("$")) return item.set(itValue.slice(1))
      if (itValue.startsWith("+")) {
        convertToNumber = true
        itValue = itValue.slice(1)
      }
      let fetchedValue = get(inputObj, itValue, defaultValue)
      if (convertToNumber) fetchedValue = +fetchedValue
      if(isNotDefined(fetchedValue)) return item.remove()
      return item.set(fetchedValue)
    } else if (isArray(itValue)) {
      let values = itValue.map(itVal => get(inputObj, itVal, defaultValue))
      return item.set(values.join(" ").trim())
    }
    return item
  })
}

// let io = {
//   name: {
//     first: "Gabin"
//     // last: "Desserprit"
//   },
//   contactDetails: {
//     phones: ["12345", "67890"],
//     email: "desserprit.gabin@gmail.com"
//   },
//   ok: true
// }

// console.log(
//   JSON.stringify(
//     morph(
//       {
//         name: "name.first",
//         lastname: "name.last"
//         // fullname: ["name.first", "name.last"],
//         // phones: {
//         //   phones: "contactDetails.phones",
//         //   phone1: "contactDetails.phones.0"
//         // }
//       },
//       io
//       // null
//     ),
//     null,
//     2
//   )
// )
