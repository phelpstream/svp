import regexes from "../consts/regexes"
const { rgxHasLeadingDot, rgxPropName, rgxEscapeChar } = regexes

export default function toPath(str) {
  var result = []
  if (rgxHasLeadingDot.test(str)) result.push("")
  str.replace(rgxPropName, function(match, number, quote, str) {
    result.push(quote ? str.replace(rgxEscapeChar, "$1") : number || match)
  })
  return result
}
