export default function partial(fn, arg1) {
  return function(...args) {
    return fn(arg1, ...args)
  }
}
