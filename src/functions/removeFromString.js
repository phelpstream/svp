import removeFromArray from "./removeFromArray"

export default function removeFromString(value, index) {
  return removeFromArray(Array.from(value), index).join("")
}
