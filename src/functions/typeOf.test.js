import { expect } from "chai"

import typeOf from "./typeOf"

describe("typeOf.js", () => {
  it("should detect that a function is of type function", () => {
    expect(typeOf(() => {})).to.be.equal("function")
  })
  it("should detect that a date object is of type date", () => {
    expect(typeOf(new Date())).to.be.equal("date")
  })
  it("should detect that an array is of type array", () => {
    expect(typeOf([])).to.be.equal("array")
  })
  it("should detect that a regex is of type regexp", () => {
    expect(typeOf(/d+/g)).to.be.equal("regexp")
  })
  it("should detect that an error is of type error", () => {
    expect(typeOf(new Error())).to.be.equal("error")
  })
  // not
  it("should not detect in strict mode that a date object is of type date", () => {
    expect(typeOf(new Date(), true)).to.be.equal("object")
  })
})
