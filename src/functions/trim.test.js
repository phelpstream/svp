import { expect } from "chai"

import trim from "./trim"

describe("trim.js", () => {
  it("should trim spaces from a string", () => {
    let obj = " Mot     "
    let testResult = trim(obj)
    expect(testResult).to.be.eql(obj.trim())
  })
})
