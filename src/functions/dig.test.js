import { expect } from "chai"

import get from "./get"
import dig from "./dig"

describe("dig.js", () => {
  it("should set uppercases recursively through an object", () => {
    let value = {
      one: {
        a: "a",
        b: "b",
        c: "c"
      },
      two: {
        a: "a",
        b: "b"
      },
      three: "t"
    }
    let iteratee = (val, key, path) => (typeof val === "string" ? val.toUpperCase() : val) // eslint-disable-line no-unused-vars
    let uppercasedObj = dig(value, iteratee)
    expect(uppercasedObj).to.be.eql({
      one: {
        a: "A",
        b: "B",
        c: "C"
      },
      two: {
        a: "A",
        b: "B"
      },
      three: "T"
    })
  })
  it("should iterate recursively through an object", () => {
    let value = {
      one: {
        a: true,
        b: true,
        c: false
      },
      two: {
        a: true,
        b: false
      },
      three: true
    }
    let iteratee = (val, key, path) => {
      expect(val).to.be.eql(get(value, path))
    }
    dig(value, iteratee)
  })
  it("should iterate recursively through an array of objects", () => {
    let value = [
      {
        one: {
          a: true,
          b: true,
          c: false
        }
      },
      {
        two: {
          a: true,
          b: false
        }
      },
      { three: true }
    ]
    let iteratee = (val, key, path) => {
      expect(val).to.be.eql(get(value, path))
    }
    dig(value, iteratee)
  })

  it("should remove the even numbers recursively", () => {
    let value = { a: 1, b: 2, c: 3, d: { d1: 5, d2: 8, d3: { d3a: 8, d3b: 5 } } }
    let removeEvenNb = item => (item.value() % 2 === 0 ? item.remove() : item)
    expect(dig(value, v => v, removeEvenNb)).to.be.eql({
      a: 1,
      c: 3,
      d: {
        d1: 5,
        d3: {
          d3b: 5
        }
      }
    })
  })
})
