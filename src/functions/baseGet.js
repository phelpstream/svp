"use strict"
import isKey from "./isKey"

export default function baseGet(obj, key) {
  if (isKey(key, obj)) return obj[key]
  return null
}
