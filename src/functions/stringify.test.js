import { expect } from "chai"

import stringify from "./stringify"

describe("stringify.js", () => {
  it("should stringify an object", () => {
    let obj = { ok: "good", no: "bad" }
    expect(stringify(obj)).to.be.equal('{"ok":"good","no":"bad"}')
  })
})
