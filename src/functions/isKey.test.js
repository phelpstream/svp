import { expect } from "chai"

import isKey from "./isKey"

describe("isKey.js", () => {
  it("should find a string key belongs to the object", () => {
    expect(isKey("ok", { ok: true })).to.be.true
  })
  it("should find a number key belongs to the object", () => {
    expect(isKey(1, { 1: true })).to.be.true
  })
  it("should find that a key doesn't belongs to the object", () => {
    expect(isKey("no", { ok: true })).to.be.false
  })
})
