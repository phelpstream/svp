export default {
  NUMBER: {
    INFINITY: Infinity,
    INFINITY_CALC: 42 / +0,
    MINUS_INFINITY_CALC: 42 / -0,
    INTEGER: 12345,
    FLOAT: 1.2345,
    LONG_FLOAT: 1234.5678901234567890123456,
    ODD: 1347,
    EVEN: 2388
  },
  BOOLEAN: {
    TRUE: true,
    FALSE: false
  },
  STRING: {
    EMPTY: "",
    SIMPLE: "That's a long sentence \n with another line."
  },
  OBJECT: {
    EMPTY: {},
    SIMPLE: { one: 1, 2: "two", ok: true, no: false }
  },
  ARRAY: {
    EMPTY: [],
    STRING: ["one", "two", "three", "four", "five"],
    INTEGER: [1395, 392, 405, 2, 3596],
    UNDEFINED: [undefined, undefined, undefined]
  },
  MAP: {
    INSTANCE: new Map(), // eslint-disable-line no-undef
    SIMPLE: function() {
      let map = new Map() // eslint-disable-line no-undef
      map.add("ok", true)
      return map
    }
  },
  WEAKMAP: {
    INSTANCE: new WeakMap(), // eslint-disable-line no-undef
    SIMPLE: function() {
      let weakMap = new WeakMap() // eslint-disable-line no-undef
      weakMap.add("ok", true)
      return weakMap
    }
  },
  SET: {
    INSTANCE: new Set(), // eslint-disable-line no-undef
    SIMPLE: function() {
      let set = new Set() // eslint-disable-line no-undef
      set.add("ok")
      return set
    }
  },
  WEAKSET: {
    INSTANCE: new WeakSet(), // eslint-disable-line no-undef
    SIMPLE: function() {
      let weakSet = new WeakSet() // eslint-disable-line no-undef
      weakSet.add({})
      return weakSet
    }
  },
  NULL: null,
  UNDEFINED: undefined
}
