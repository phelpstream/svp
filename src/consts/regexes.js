export default {
  rgxIsDeepProp: /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
  rgxIsPlainProp: /^\w*$/,
  rgxHasLeadingDot: /^\./,
  rgxPropName: /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g,
  rgxEscapeChar: /\\(\\)?/g
}
