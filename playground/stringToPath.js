const rgxIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
  rgxIsPlainProp = /^\w*$/,
  rgxHasLeadingDot = /^\./,
  rgxPropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g,
  rgxEscapeChar = /\\(\\)?/g

/**
 * Converts `string` to a property path array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the property path array.
 */
var stringToPath = function(string) {
  var result = []
  if (rgxHasLeadingDot.test(string)) {
    result.push("")
  }
  string.replace(rgxPropName, function(match, number, quote, string) {
    console.log(match, number, quote, string)
    result.push(quote ? string.replace(rgxEscapeChar, "$1") : number || match)
  })
  return result
}

console.log(stringToPath("super[2].cool.doc"))
